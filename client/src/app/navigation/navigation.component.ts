import { Component } from '@angular/core';



@Component( {
    selector: 'navigation',
    template: `<nav md-tab-nav-bar>
                        <a [disabled]="!isClassesAvailable()" md-tab-link [routerLink]="['/school-classes']" routerLinkActive
                        #rla="routerLinkActive">Classes</a>

                        <a [disabled]="!isChildrenAvailable()" md-tab-link [routerLink]="['/children']" routerLinkActive
                        #rla="routerLinkActive">Children</a>

                        <a [disabled]="!isLessonEventAvailable()" md-tab-link [routerLink]="['/list-lesson-event-2']" routerLinkActive
                        #rla="routerLinkActive">Lesson event</a>

                        <a [disabled]="!isTasksAvailable()" md-tab-link [routerLink]="['/tasks']" routerLinkActive
                        #rla="routerLinkActive">Tasks</a>
                        
                        <a [disabled]="!isChildrenAvailable()" md-tab-link [routerLink]="['/marksBySubject']" routerLinkActive
                        #rla="routerLinkActive">Marks by subject</a>
                        
                        <a md-tab-link [routerLink]="['/login']">Logout</a>
                </nav>`
} )

export class NavigationComponent {

    constructor() { }


    userRole() {

        let currentUser = localStorage.getItem( 'currentUser' );

        if (currentUser == null) return "";

        const jwtData = currentUser.split( '.' )[1];
        const decodedJwtJsonData = window.atob( jwtData );
        const decodedJwtData = JSON.parse( decodedJwtJsonData );

        const role = decodedJwtData.authority[0].authority;
        return role;

    }

    isClassesAvailable()
    {
        let role = this.userRole();
        return role === "ADMIN";
    }

    isChildrenAvailable()
    {
        let role = this.userRole();
        return role === "PARENT";
    }

    isTasksAvailable()
    {
        let role = this.userRole();
        return role === "CHILD";
    }
    isLessonEventAvailable() {
      let role = this.userRole();
      return role === "TEACHER";
    }

}



