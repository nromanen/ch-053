export class Teacher {
  constructor(
    public firstName: string,
    public lastName: string,
    public patronymic: string,
    public nickname: string,
    public email: string,
    public telephone: string
  ) { }
}
