import { Component, OnInit } from '@angular/core';
import {MdDialog} from "@angular/material";
import {AddTeacherDialogComponent} from "./add-teacher-dialog/add-teacher-dialog.component";

@Component({
  selector: 'app-teacher-table',
  templateUrl: './teacher-table.component.html',
  styleUrls: ['./teacher-table.component.css']
})
export class TeacherTableComponent implements OnInit {

  constructor(public dialog: MdDialog) { }

  ngOnInit() {
  }

  toAddTeacher() {
    this.dialog.open(AddTeacherDialogComponent, {width: '600px', data: 'No free teacher for the curator'});
  }
}
