import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {MdDialog} from "@angular/material";
import {AppService} from "../../app.service";
import {Teacher} from "../teacher.model";

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const NAME = '[A-Z][a-z]+';
const TELEPHONE = '[0-9]{9}'

@Component({
  selector: 'app-add-teacher-dialog',
  templateUrl: './add-teacher-dialog.component.html',
  styleUrls: ['./add-teacher-dialog.component.css']
})
export class AddTeacherDialogComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)]);
  firstNameFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(NAME)]);
  lastNameFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(NAME)]);
  patronymicNameFormControl = new FormControl('', [
    Validators.pattern(NAME)]);
  nicknameNameFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)]);
  telephoneFormControl = new FormControl('', [
    Validators.pattern(TELEPHONE)]);
  constructor(private service: AppService, public dialog: MdDialog) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialog.closeAll();
  }

  addTeacher(firstName: string, lastName: string, patronymic: string, nickname: string, email: string, telephone: string) {
    if (this.firstNameFormControl.valid && this.lastNameFormControl.valid && this.patronymicNameFormControl.valid && this.nicknameNameFormControl.valid && this.emailFormControl.valid && this.telephoneFormControl.valid) {
      this.service.postJSON(new Teacher(firstName, lastName, patronymic, nickname, email, telephone), 'api/admin/teacher/createSchoolClass')
    }
  }
}
