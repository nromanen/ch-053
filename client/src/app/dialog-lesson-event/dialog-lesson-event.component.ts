import {Component, Inject, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {MD_DIALOG_DATA, MdDialog} from "@angular/material";

@Component({
  selector: 'app-dialog-lesson-event',
  templateUrl: './dialog-lesson-event.component.html',
  styleUrls: ['./dialog-lesson-event.component.css']
})
export class DialogLessonEventComponent {

  public _router: Router;
  constructor(@Inject(MD_DIALOG_DATA) public data: any, _router: Router, public dialog: MdDialog) {
    this._router = _router;
  }

  goToClasses() {
    this._router.navigate(['/list-lesson-event-2']);
  }

}
