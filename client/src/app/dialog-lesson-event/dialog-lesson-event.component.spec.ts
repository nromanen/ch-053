import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogLessonEventComponent } from './dialog-lesson-event.component';

describe('DialogLessonEventComponent', () => {
  let component: DialogLessonEventComponent;
  let fixture: ComponentFixture<DialogLessonEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogLessonEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogLessonEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
