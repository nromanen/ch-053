import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {
  DemoFormComponent
} from './demo-form/demo-form';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {NavigationComponent} from './navigation/navigation.component';
import {
  RouterModule, Routes
} from '@angular/router';
import {HttpModule} from '@angular/http';
import {AppService} from './app.service';
import {ParentsDropdownComponent} from './parents-dropdown/parents-dropdown.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MdAutocompleteModule,
  MdButtonModule,
  MdButtonToggleModule,
  MdCardModule,
  MdCheckboxModule,
  MdChipsModule,
  MdCoreModule,
  MdDatepickerModule,
  MdDialogModule,
  MdExpansionModule,
  MdGridListModule,
  MdIconModule,
  MdInputModule,
  MdListModule,
  MdMenuModule,
  MdNativeDateModule,
  MdPaginatorModule,
  MdProgressBarModule,
  MdProgressSpinnerModule,
  MdRadioModule,
  MdRippleModule,
  MdSelectModule,
  MdSidenavModule,
  MdSliderModule,
  MdSlideToggleModule,
  MdSnackBarModule,
  MdSortModule,
  MdTableModule,
  MdTabsModule,
  MdToolbarModule,
  MdTooltipModule,

} from '@angular/material';

import {ChildTaskComponent} from './app-child/child-task.component';
import {TaskComponent} from './app-child/task/task.component';
import {CdkTableModule} from '@angular/cdk/table';
import {DialogOverviewExampleDialog} from "./parent/child-table/marks-journal/marks-journal.component";
import {ChildTableComponent} from "./parent/child-table/child-table.component";
import {MarksJournalComponent} from "./parent/child-table/marks-journal/marks-journal.component";
import {CommentDialogComponent} from './app-child/comment-dialog/comment-dialog.component';
import {FileDialogComponent} from './app-child/file-dialog/file-dialog.component';
import {SafePipe} from './app-child/file-dialog/safe.pipe';
import {ErrorDialogComponent} from "./admin/school-class-table-2/add-school-class-dialog/error-dialog/error-dialog.component";
import {DialogComponent} from "./admin/school-class-table-2/add-school-class-dialog/dialog-succes/dialog.component";
import {SchoolClassTable2Component} from "./admin/school-class-table-2/school-class-table-2.component";
import {AddSchoolClassDialogComponent} from "./admin/school-class-table-2/add-school-class-dialog/add-school-class-dialog.component";
import {DialogWarningComponent} from "./admin/school-class-table-2/add-school-class-dialog/dialog-warning/dialog-warning.component";
import {AuthGuardChildComponent} from './auth-guard-child/auth-guard-child.component';
import {AuthGuardParentComponent} from './auth-guard-parent/auth-guard-parent.component';
import {AuthGuardTeacherComponent} from './auth-guard-teacher/auth-guard-teacher.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {TeacherTableComponent} from './teacher-table/teacher-table.component';
import {AddTeacherDialogComponent} from './teacher-table/add-teacher-dialog/add-teacher-dialog.component';
import {LessonEventComponent} from './lesson-event/lesson-event.component';
import {AuthGuardAdminComponent} from "./auth-guard-admin/auth-guard-admin.component";
import {AuthGuard} from "./_guards/auth.guard";
import {HttpClient} from "./_services/HttpClient";
import {AuthenticationService} from "./_services/authentication.service";
import {ChildByClassTableComponent} from "./lesson-event/child-by-class-table/child-by-class-table.component";
import {DialogSuccesComponent} from "./lesson-event/child-by-class-table/dialog-succes/dialog-succes.component";
import { MarksBySubjectComponent, DialogOverviewExampleDialogForSubject } from "./parent/child-table-mark-by-subject/marks-by-subject/marks-by-subject.component";
import { ChildTableMarkBySubjectComponent } from "./parent/child-table-mark-by-subject/child-table-mark-by-subject.component";
import { UpdateLessonEventComponent } from './update-lesson-event/update-lesson-event.component';
import { ChildmarkUpdateComponent } from './update-lesson-event/childmark-update/childmark-update.component';
import { ListLessonEvent2Component } from './list-lesson-event-2/list-lesson-event-2.component';
import {DialogYesOrNoComponent} from "./dialog-yes-or-no/dialog-yes-or-no.component";
import { DialogLessonEventComponent } from './dialog-lesson-event/dialog-lesson-event.component';





const routes: Routes = [
  {path: '', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'add-school-class', redirectTo: 'school-classes'},
  {path: 'children', component: ChildTableComponent, canActivate: [AuthGuardParentComponent]},
  {path: 'marksBySubject', component: ChildTableMarkBySubjectComponent, canActivate: [AuthGuardParentComponent]},
  {path: 'form', component: DemoFormComponent},
  {path: 'error', component: ErrorDialogComponent},
  {path: 'dialog', component: DialogComponent},
  {path: 'teachers', component: TeacherTableComponent},
  {path: 'tasks', component: ChildTaskComponent, canActivate: [AuthGuardChildComponent]},
  {path: 'school-classes', component: SchoolClassTable2Component, canActivate: [AuthGuardAdminComponent]},
  {path: 'login', component: LoginComponent},
  {path: 'task-comment-dialog', component: CommentDialogComponent},
  {path: 'task-file-dialog', component: FileDialogComponent},
  {path: 'add-school-class', component: AddSchoolClassDialogComponent},
  {path: 'lesson-event', component: LessonEventComponent, canActivate: [AuthGuardTeacherComponent]},
  {path: 'list-lesson-event-2', component: ListLessonEvent2Component, canActivate: [AuthGuardTeacherComponent]},
  {path: 'update-lesson-event', component: UpdateLessonEventComponent, canActivate: [AuthGuardTeacherComponent]},
  {path: 'app-child-by-class-table', component: ChildByClassTableComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({

  declarations: [
    AppComponent,
    DemoFormComponent,
    NavigationComponent,
    ParentsDropdownComponent,
    NavigationComponent,
    ErrorDialogComponent,
    DialogComponent,
    MarksJournalComponent,
    DialogOverviewExampleDialog,
    ChildTaskComponent,
    TaskComponent,
    ChildTableComponent,
    SchoolClassTable2Component,
    DialogWarningComponent,
    AddSchoolClassDialogComponent,
    CommentDialogComponent,
    FileDialogComponent,
    SafePipe,
    LoginComponent,
    HomeComponent,
    TeacherTableComponent,
    AddTeacherDialogComponent,
    LessonEventComponent,
    ChildByClassTableComponent,
    DialogSuccesComponent,
    MarksBySubjectComponent,
    ChildTableMarkBySubjectComponent,
    DialogOverviewExampleDialogForSubject,
    UpdateLessonEventComponent,
    ChildmarkUpdateComponent,
    ListLessonEvent2Component,
    DialogYesOrNoComponent,
    DialogLessonEventComponent
  ],

  entryComponents: [DialogOverviewExampleDialog, DialogWarningComponent, DialogComponent, DialogLessonEventComponent, AddSchoolClassDialogComponent, DialogSuccesComponent, DialogOverviewExampleDialogForSubject, DialogYesOrNoComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    HttpModule,
    MdAutocompleteModule,
    MdButtonModule,
    MdButtonToggleModule,
    MdCardModule,
    MdCheckboxModule,
    MdChipsModule,
    MdCoreModule,
    MdDatepickerModule,
    MdDialogModule,
    MdExpansionModule,
    MdGridListModule,
    MdIconModule,
    MdInputModule,
    MdListModule,
    MdMenuModule,
    MdNativeDateModule,
    MdPaginatorModule,
    MdProgressBarModule,
    MdProgressSpinnerModule,
    MdRadioModule,
    MdRippleModule,
    MdSelectModule,
    MdSidenavModule,
    MdSliderModule,
    MdSlideToggleModule,
    MdSnackBarModule,
    MdSortModule,
    MdTableModule,
    MdTabsModule,
    MdToolbarModule,
    MdTooltipModule
  ],

  providers: [
    {provide: AppService, useClass: AppService},
    AuthenticationService,
    AuthGuard,
    AuthGuardAdminComponent,
    AuthGuardChildComponent,
    AuthGuardParentComponent,
    AuthGuardTeacherComponent,
    HttpClient,
  ],

  bootstrap: [AppComponent]
})
export class AppModule {
}
