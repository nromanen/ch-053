﻿﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from "rxjs/Observable";
import { AuthenticationService } from "../_services/authentication.service";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor( private router: Router ) { }

    canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {
        // This if will go after taking token from local storage and pulling role from it for itch of user roles
        const jwt = localStorage.getItem( 'currentUser' );
        if (jwt) {
          const jwtData = jwt.split( '.' )[1];
          const decodedJwtJsonData = window.atob( jwtData );
          const decodedJwtData = JSON.parse( decodedJwtJsonData );

          const role = decodedJwtData.authority[0].authority;

          if (role === 'ADMIN') {
            this.router.navigate(['/school-classes']);
            return true;
          } else if (role === 'PARENT') {
            this.router.navigate(['/children']);
            return true;
          }
          else if (role === 'TEACHER') {
            this.router.navigate(['/list-lesson-event-2']);
            return true;
          }
          else if (role === 'CHILD') {
            this.router.navigate(['/tasks']);
            return true;
          }
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}
