import { Component, OnInit } from '@angular/core';
import {AppSettings} from "../app.settings";
import {HttpClient} from "../_services/HttpClient";
import {ActivatedRoute, Router} from "@angular/router";
import {
  ChildByClassTableComponent, ChildClassServer,
  ExampleDataSource
} from "../lesson-event/child-by-class-table/child-by-class-table.component";
import {ChildMarkModel} from "../lesson-event/child-by-class-table/child-mark.model";
import {ChildmarkUpdateComponent} from "./childmark-update/childmark-update.component";
import {DialogYesOrNoComponent} from "../dialog-yes-or-no/dialog-yes-or-no.component";
import {MdDialog, MdIconRegistry} from "@angular/material";
import {DomSanitizer} from "@angular/platform-browser";


@Component({
  selector: 'app-update-lesson-event',
  templateUrl: './update-lesson-event.component.html',
  styleUrls: ['./update-lesson-event.component.css']
})
export class UpdateLessonEventComponent implements OnInit {
  lessonEvent;
  schoolClassName;
  schoolClassId;
  subjectName;
  eventDate;
  lessonEventType;
  comments;
  teacherId = this.getUserId();
  sub;
  id;
  studentsBySchoolClass;
  childrenMarks: ChildMarkModel[];

  private http: HttpClient;
  constructor(http: HttpClient, private route: ActivatedRoute,
              private router: Router, public dialog: MdDialog) {
    this.http = http;
  }
  ngOnInit() {
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this.id = params['id'];
      });
    this.http.get(AppSettings.URL + AppSettings.lessonEventFind(this.id)).subscribe(
      response => {this.lessonEvent = response.json();
      this.schoolClassName = this.lessonEvent.schoolClassName;
      this.subjectName = this.lessonEvent.subjectName;
      this.eventDate = this.lessonEvent.eventDate;
      this.comments = this.lessonEvent.comment;
      this.schoolClassId = this.lessonEvent.schoolClassId;
      ChildmarkUpdateComponent.schoolClassId = this.schoolClassId;
      ChildmarkUpdateComponent.lessonId = this.lessonEvent.lessonId;
      ChildmarkUpdateComponent.comment = this.comments;
      ChildmarkUpdateComponent.date = this.eventDate;
      ChildmarkUpdateComponent.lessonEventId = this.lessonEvent.id;
        this.studentsBySchoolClass = true;
        this.http.get(AppSettings.URL + AppSettings.childrenMarkByLessonEventId(ChildmarkUpdateComponent.lessonEventId))
          .subscribe(response => { console.log(this.id); this.childrenMarks = response.json(); console.log(this.childrenMarks); this.lessonEventType = response.json()[0].lessonEventTypeName; console.log(this.lessonEventType)});
      });

  }
  getUserId(): string {
    const jwt = localStorage.getItem('currentUser');
    const jwtData = jwt.split('.')[1];
    const decodedJwtJsonData = window.atob(jwtData);
    const decodedJwtData = JSON.parse(decodedJwtJsonData);
    return decodedJwtData.userid;
  }
  goBack() {
    this.dialog.open(DialogYesOrNoComponent, {width: '250px', data: 'Are you sure?'});
  }
}
