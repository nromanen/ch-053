import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {HttpClient} from "../../_services/HttpClient";
import {ChildMarkModel} from "../../lesson-event/child-by-class-table/child-mark.model";
import {MdDialog} from "@angular/material";
import {AppSettings} from "../../app.settings";
import {DialogWarningComponent} from "../../admin/school-class-table-2/add-school-class-dialog/dialog-warning/dialog-warning.component";
import {Lesson} from "../../lesson-event/lesson-event.model";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {DialogLessonEventComponent} from "../../dialog-lesson-event/dialog-lesson-event.component";

const MARK_CONTROL1 = "(^[1-9][0-9]?)|(0)|(100)";

@Component({
  selector: 'app-childmark-update',
  templateUrl: './childmark-update.component.html',
  styleUrls: ['./childmark-update.component.css']
})
export class ChildmarkUpdateComponent implements OnInit {

  @ViewChild('filter') filter: ElementRef;

  markControl = new FormControl('', [
    Validators.pattern(MARK_CONTROL1)]);
  sc: ChildClassServer;
  displayedColumns = ['name', 'absent', 'score'];
  dataSource : ExampleDataSource | null;
  types = ['Present', 'Absent'];
  http: HttpClient;
  absent = false;
  static schoolClassId: number;
  static lessonEventTypeId: string;
  static lessonId: string;
  static lessonEventId: string;
  static date: string;
  static comment: string;
  absentForBase: string;
  children: ChildMarkModel[];
  childMark: ChildMarkModel;
  childrenMarks: ChildMarkModel[];
  newDate: Date;
  dateFinish;
  completionDateDay;
  completionDate;
  lessonEventType: string;
  constructor(http: HttpClient, public dialog: MdDialog) {
    this.http = http;
    this.sc = new ChildClassServer(this.http);
    this.children = this.sc.dataChange.getValue();
    //this.http.get(AppSettings.URL + AppSettings.childrenMarkByLessonEventId(ChildByClassTableComponent.lessonEventId)).subscribe(response => this.childrenMarks = response.json());
  }
  ngOnInit() {
    this.http.get(AppSettings.URL + AppSettings.childrenMarkByLessonEventId(ChildmarkUpdateComponent.lessonEventId))
      .subscribe(response => {this.childrenMarks = response.json(); this.dataSource = new ExampleDataSource(this.sc);});
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) { return; }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
  defaultAbsent(id) : string {
    if (this.childrenMarks.find(childMark => childMark.id === id).absent === 'true') {
      return 'Absent'
    }
    else {
      return 'Present';
    }
  }
  selectAbsent(type: string, id: string) {
    this.children = this.sc.dataChange.getValue();
    if (type === 'Absent') {
      this.absent = true;
      this.absentForBase = 'true'
    }
    else {
      this.absent = false;
      this.absentForBase = 'false';
    }
    this.childrenMarks.find(childMark => childMark.id === id).absent = this.absentForBase;
    console.log(this.childrenMarks.find(childMark => childMark.id === id).absent);
    this.http.post(AppSettings.URL + AppSettings.CHILD_MARK_UPDATE, this.childrenMarks.find(childMark => childMark.id === id))
      .subscribe(ob => console.log('yes'),
        error => this.dialog.open(DialogWarningComponent, {
          width: '250px',
          data: error.json()}),
        () => console.log('dasdasdas')
      );
  }
  onSelectAbsent(id: string) {
    return this.childrenMarks.find(childMark => childMark.id === id).absent === 'true';
  }
  onSelectMark(id: string) {
    if (this.childrenMarks.find(childMark => childMark.id === id).mark != null) {
      return true;
    }
    else {
      return false;
    }
  }
  alertStyles;
  /*finishLesson() {
    this.newDate = new Date;
    this.completionDateDay = this.newDate.getDate().toString();
    if (this.completionDateDay.length === 1) {
      this.completionDateDay = '0' + this.completionDateDay;
    }
    this.completionDate = this.newDate.getUTCMonth() + 1 + '/' + this.completionDateDay + '/' + this.newDate.getUTCFullYear();
    console.log(ChildmarkUpdateComponent.lessonEventId);
    this.http.post(AppSettings.URL + AppSettings.LESSON_EVENT_UPDATE, new Lesson(ChildmarkUpdateComponent.lessonEventId, ChildmarkUpdateComponent.lessonId, ChildmarkUpdateComponent.date, ChildmarkUpdateComponent.comment, ChildmarkUpdateComponent.lessonEventTypeId, 'true', this.completionDate))
      .subscribe(ob => console.log('yes'),
        error => this.dialog.open(DialogWarningComponent, {
          width: '250px',
          data: error.json()}),
        () =>  this.dialog.open(DialogComponent, {width: '250px', data: 'Your lesson event finished!'})
      );
  }*/

  finishLesson() {
    this.newDate = new Date;
    this.completionDateDay = this.newDate.getDate().toString();
    if (this.completionDateDay.length === 1) {
      this.completionDateDay = '0' + this.completionDateDay;
    }
    this.completionDate = this.newDate.getUTCMonth() + 1 + '/' + this.completionDateDay + '/' + this.newDate.getUTCFullYear();
    this.http.post(AppSettings.URL + AppSettings.LESSON_EVENT_UPDATE, new Lesson(ChildmarkUpdateComponent.lessonEventId, ChildmarkUpdateComponent.lessonId, ChildmarkUpdateComponent.date, ChildmarkUpdateComponent.comment, ChildmarkUpdateComponent.lessonEventTypeId, 'true', this.completionDate))
      .subscribe(ob => console.log('yes'),
        error => this.dialog.open(DialogWarningComponent, {
          width: '250px',
          data: error.json()}),
        () =>  this.dialog.open(DialogLessonEventComponent, {width: '250px', data: 'Your lesson event finished!'})
      );
  }

  addMark(id, mark) {
    if (mark != "" && this.markControl.valid && mark >= 0 && mark <= 100) {
      this.childrenMarks.find(childMark => childMark.id === id).mark = mark;
      this.http.post(AppSettings.URL + AppSettings.CHILD_MARK_UPDATE, this.childrenMarks.find(childMark => childMark.id === id))
        .subscribe(ob => console.log('yes'),
          error => this.dialog.open(DialogWarningComponent, {
            width: '250px',
            data: error.json()
          })
        );
    }
    else {
      this.dialog.open(DialogWarningComponent, {
        width: '250px',
        data: 'Mark must be from 0 to 100.'
      })
    }
  }
}

/**
 * Data source to provide what data should be rendered in the table. The observable provided
 * in connect should emit exactly the data that should be rendered by the table. If the data is
 * altered, the observable should emit that new set of data on the stream. In our case here,
 * we return a stream that contains only one set of data that doesn't change.
 */
export class ChildClassServer {
  startLesson() {}
  dataChange: BehaviorSubject<ChildMarkModel[]> = new BehaviorSubject<ChildMarkModel[]>([]);
  get data(): ChildMarkModel[] { return this.dataChange.value; }
  public http: HttpClient;
  constructor(http: HttpClient) {
    this.http = http;
    this.http.get(AppSettings.URL + AppSettings.childrenMarkByLessonEventId(ChildmarkUpdateComponent.lessonEventId)).subscribe(response => this.dataChange.next(response.json()));
  }
}


export class ExampleDataSource extends DataSource<any> {
  _filterChange = new BehaviorSubject('');
  get filter(): string { return this._filterChange.value; }
  set filter(filter: string) { this._filterChange.next(filter); }

  constructor(private sc: ChildClassServer) {
    super();
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<ChildMarkModel[]> {
    const displayDataChanges = [
      this.sc.dataChange,
      this._filterChange,
    ];

    return Observable.merge(...displayDataChanges).map(() => {
      return this.sc.data.slice().filter((item: ChildMarkModel) => {
        let searchStr = (item.firstName + item.lastName).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) != -1;
      });
    });
  }

  disconnect() {}
}
