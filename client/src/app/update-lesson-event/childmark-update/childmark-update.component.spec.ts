import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildmarkUpdateComponent } from './childmark-update.component';

describe('ChildmarkUpdateComponent', () => {
  let component: ChildmarkUpdateComponent;
  let fixture: ComponentFixture<ChildmarkUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildmarkUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildmarkUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
