import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateLessonEventComponent } from './update-lesson-event.component';

describe('UpdateLessonEventComponent', () => {
  let component: UpdateLessonEventComponent;
  let fixture: ComponentFixture<UpdateLessonEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateLessonEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateLessonEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
