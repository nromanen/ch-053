import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {AppSettings} from './app.settings';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from "./_services/HttpClient";

@Injectable()
export class AppService {
  constructor(private http: HttpClient) {}
  postJSON(obj: Object, urlApi: String) {
    const body = JSON.stringify(obj);
    return this.http.post( AppSettings.URL + urlApi, body)
      .map(res => res.json());
  }
  /*public getdata(start:Number):any{
    return this.http.get(`${AppSettings.URL}${start}`)
      .map((response:Response) => response.json())
      .catch((error:any) => Observable.throw(error.json().error) || 'Server Error');
  }*/
}
