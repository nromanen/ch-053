import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialog} from "@angular/material";
import {Router, ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {
  public _router: Router;
  constructor(@Inject(MD_DIALOG_DATA) public data: any, _router: Router, public dialog: MdDialog) {
    this._router = _router;
  }

  goToClasses() {
    window.location.reload();
    this.dialog.closeAll();
  }
}
