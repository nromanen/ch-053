import {Component, NgZone} from '@angular/core';
import {DialogComponent} from "./dialog-succes/dialog.component";
import {FormControl, Validators} from "@angular/forms";
import {AppService} from "../../../app.service";
import {Exception} from "../../../exception.model";
import {HttpClient} from "../../../_services/HttpClient";
import {Router} from "@angular/router";
import {MdDialog} from "@angular/material";
import {AppSettings} from "../../../app.settings";
import {DialogWarningComponent} from "./dialog-warning/dialog-warning.component";
import {SchoolClass} from "../school-class";

const NAME_SCHOOL_CLASS_1_to_9 = "(^[1-9][a-z]?)|(^[1][0-2][a-z]?)";

@Component({
  selector: 'app-add-school-class-dialog',
  templateUrl: './add-school-class-dialog.component.html',
  styleUrls: ['./add-school-class-dialog.component.css']
})
export class AddSchoolClassDialogComponent{
  classNameFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(NAME_SCHOOL_CLASS_1_to_9)]);
  selectControl = new FormControl('', [Validators.required]);
  selectedValue;

  private idTeacher: string;
  teachers;
  private exception: Exception;
  constructor(private service: AppService, private http: HttpClient, public router: Router, public dialog: MdDialog, public zone: NgZone) {
    this.http.get(AppSettings.URL + AppSettings.TEACHER_READ_NOT_CURATOR).subscribe(response => this.teachers = response.json());
  }
  onSelect(idTeacher: string) {
    this.idTeacher = idTeacher;
  }
  addClass(name: string) {
    if (this.classNameFormControl.valid && this.selectControl.valid) {
      //this.http.post(AppSettings.URL + AppSettings.CREATE_SCHOOL_CLASS, new SchoolClass(name, this.idTeacher)).subscribe(res => res.json());
      this.service.postJSON(new SchoolClass(name, this.idTeacher), AppSettings.SCHOOL_CLASS_CREATE).subscribe(
        data => {console.log('in progress')},
        error => this.dialog.open(DialogWarningComponent, {
          width: '250px',
          data: error.json()}),
        () => this.completeAddClass()
      );
    }
  }
  completeAddClass() {
    this.dialog.open(DialogComponent, {width: '250px', data: 'Your class has been created!'});
  }
  unselect(): void {
    this.selectedValue = undefined;
  }
  onNoClick(): void {
    this.dialog.closeAll();
  }

  ngOnInit() {
  }

}
