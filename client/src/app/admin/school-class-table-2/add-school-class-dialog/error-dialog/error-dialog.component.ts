import {Component, Inject} from '@angular/core';
import {MD_DIALOG_DATA, MdDialog, MdDialogRef} from "@angular/material";
import {DialogComponent} from "../dialog-succes/dialog.component";

@Component({
  selector: 'app-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.css']
})
export class ErrorDialogComponent {
  constructor(public dialog: MdDialog) {}

  openDialog() {
    this.dialog.open(DialogComponent, {
      data: {
        animal: 'panda',
      }
    });
  }
}
