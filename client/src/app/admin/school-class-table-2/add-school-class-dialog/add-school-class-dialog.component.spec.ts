import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSchoolClassDialogComponent } from './add-school-class-dialog.component';

describe('AddSchoolClassDialogComponent', () => {
  let component: AddSchoolClassDialogComponent;
  let fixture: ComponentFixture<AddSchoolClassDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSchoolClassDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSchoolClassDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
