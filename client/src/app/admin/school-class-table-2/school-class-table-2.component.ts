import {Component, OnInit} from '@angular/core';
import {HttpClient} from '../../_services/HttpClient';
import 'rxjs/Rx';

import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {DataSource} from '@angular/cdk/table';
import {AppSettings} from '../../app.settings';
import {SchoolClass} from './school-classes-table.model';
import {MdDialog} from '@angular/material';
import {AddSchoolClassDialogComponent} from './add-school-class-dialog/add-school-class-dialog.component';
import {DialogWarningComponent} from './add-school-class-dialog/dialog-warning/dialog-warning.component';



@Component({
  selector: 'app-school-class-table-2',
  templateUrl: './school-class-table-2.component.html',
  styleUrls: ['./school-class-table-2.component.css']
})
export class SchoolClassTable2Component  implements OnInit {
  sc: SchoolClassesServer;
  displayedColumns = ['name', 'curator'];
  dataSource : SchoolClassDataSource | null;
  public begin: number = 0;
  public count: number = 10;
  notFreeTeacher: boolean = false;
  teachers = [{id: 1, firstName: 'Marina'}];
  public countTeacher: number;
  http: HttpClient;
  constructor(http: HttpClient,  public dialog: MdDialog) {
    this.http = http;
    this.http.get(AppSettings.URL + AppSettings.TEACHER_READ_NOT_CURATOR).subscribe(response => this.teachers = response.json());
    this.countTeacher = this.teachers.length;
    this.sc = new SchoolClassesServer(http, this.begin, this.count);
    this.dataSource = new SchoolClassDataSource(this.sc);
    /*this.http.get(AppSettings.URL + `/api/admin/school-classes/searchLesson?start=0&count=100`).
    subscribe(response => { if (response.json().length === this.sc.dataChange.getValue().length) { this.b = true}});*/
  }
  toAddClass() {
    if (!this.notFreeTeacher) {
      this.dialog.open(AddSchoolClassDialogComponent, {width: '600px', data: 'No free teacher for the curator'});
    }
  }
  freeTeacherForAddClass() : boolean {
    if (this.teachers.length === 0) {
      this.notFreeTeacher = true;
      return true;
    }
    else {
      return false;
    }
  }
  overMouseAddClass() {
    if (this.notFreeTeacher) {

      this.dialog.open(DialogWarningComponent, {width: '250px', data: 'No free teacher for the curator'});
    }
  }

  nextPage() {
    this.begin = this.begin + this.count;
    this.sc = new SchoolClassesServer(this.http, this.begin, this.count);
    this.dataSource = new SchoolClassDataSource(this.sc);
  }
  lastPage() {
    this.begin = this.begin - this.count;
    this.sc = new SchoolClassesServer(this.http, this.begin, this.count);
    this.dataSource = new SchoolClassDataSource(this.sc);
  }
  ngOnInit() {
  }
}

export class SchoolClassesServer {
  dataChange: BehaviorSubject<SchoolClass[]> = new BehaviorSubject<SchoolClass[]>([]);
  public http: HttpClient;

  constructor(http: HttpClient, begin: number, count: number) {
    this.http = http;

    this.http.get(AppSettings.URL + AppSettings.SCHOOL_CLASS_READ_ALL).subscribe(response => this.dataChange.next(response.json()));
    //this.http.get(AppSettings.URL + `/api/admin/school-classes/search?start=${begin}&count=${count}`).subscribe(response => this.dataChange.next(response.json()));
  }
}

export class SchoolClassDataSource extends DataSource<any> {
  constructor(private sc: SchoolClassesServer) {
    super();
  }
  connect(): Observable<SchoolClass[]> {
    return this.sc.dataChange;
  }
  disconnect() {}
}
