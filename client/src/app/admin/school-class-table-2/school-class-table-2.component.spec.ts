import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolClassTable2Component } from './school-class-table-2.component';

describe('SchoolClassTable2Component', () => {
  let component: SchoolClassTable2Component;
  let fixture: ComponentFixture<SchoolClassTable2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolClassTable2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolClassTable2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
