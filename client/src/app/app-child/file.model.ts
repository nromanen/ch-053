export class File {
  id: string;
  fileName: string;
  filePath: string;

  constructor(id: string, fileName: string, filePath: string) {
    this.id = id;
    this.fileName = fileName;
    this.filePath = filePath;
  }
}
