import {Mark} from './mark.model';

export class Child {
    private id: string;
    private firstName: string;
    private lastName: string;
    private marks: Mark[];

    get getId(): string {
        return this.id;
    }

    set setId(value: string) {
        this.id = value;
    }

    get getFirstName(): string {
        return this.firstName;
    }

    set setFirstName(value: string) {
        this.firstName = value;
    }

    get getLastName(): string {
        return this.lastName;
    }

    set setLastName(value: string) {
        this.lastName = value;
    }

    get getMarks(): Mark[] {
        return this.marks;
    }

    set setMarks(value: Mark[]) {
        this.marks = value;
    }

    constructor(id: string, firstName: string, lastName: string, marks: Mark[]) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.marks = marks;
    }
}
