import {File} from './file.model';

export class Mark {
  id: string;
  date: string;
  mark: string;
  subject: string;
  lessonEventTypeName: string;
  comment: string;
  files: File[];

  constructor(id: string, date: string, mark: string, subject: string, lessonEventTypeName: string, comment: string, files: File[]) {}

}
