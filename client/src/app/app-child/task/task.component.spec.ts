import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TascComponent } from './tasc.component';

describe('TascComponent', () => {
  let component: TascComponent;
  let fixture: ComponentFixture<TascComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TascComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TascComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
