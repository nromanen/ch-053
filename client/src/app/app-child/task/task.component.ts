import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from '../../_services/HttpClient';
import {DataSource} from '@angular/cdk/table';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {Mark} from '../mark.model';

@Component({
  selector: 'app-child-task',
  templateUrl: 'task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  @Input() marks: Mark[];
  dates: {[key: string]: Mark[]} = {};
  childId = '204';
  displayedProperties = {date: 'Date', subject: 'Subject', mark: 'Mark', lessonEventTypeName: 'Lesson Type', comment: 'Comment'};
  displayedColumns = Object.keys(this.displayedProperties);
  http: HttpClient;

  constructor(private _http: HttpClient) {
    this.http = _http;
  }

  getMarksOnDate(date: string): Mark[] {
    return this.dates[date];
  }

  getSetOfDates(): string[] {
    return Object.keys(this.dates);
  }

  getMarksGroupedByDate(): {[key: string]: Mark[]} {
    let setDates = new Set();
    let dates: {[key: string]: Mark[]} = {};
    this.marks.forEach(mark => setDates.add(mark.date));
    setDates.forEach(date => dates[date] = []);
    this.marks.forEach(currentMark => dates[currentMark.date].push(currentMark));
    return dates;
  }

  initTableDataSource(childMarks: Mark[]) {
    this.marks = childMarks;
    this.dates = this.getMarksGroupedByDate();
  }

  ngOnInit() {
  }
}
