import { Component, OnInit, Inject } from '@angular/core';
import { MdDialogRef, MD_DIALOG_DATA } from '@angular/material';
import { DomSanitizer, SafeResourceUrl, } from '@angular/platform-browser';
import { SafeUrl } from '@angular/platform-browser';

@Component({
    selector: 'app-file-dialog',
    templateUrl: './file-dialog.component.html',
    styleUrls: ['./file-dialog.component.css']
})
export class FileDialogComponent implements OnInit {
    url: SafeResourceUrl;
    currentUrl: SafeUrl;
    isLoaded: boolean = false;

    constructor(
        public dialogRef: MdDialogRef<FileDialogComponent>,
        @Inject(MD_DIALOG_DATA) public data: any, public sanitizer: DomSanitizer) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    ngOnInit(): void {
        this.currentUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.data.filePath);
    }

    createIframe(event) {
        event.path[1].removeChild(event.path[1].children[0]);
        let iframe = document.createElement('iframe');
        iframe.style.width = '0px';
        iframe.style.height = '0px';
        iframe.style.opacity = '0';
        iframe.src = this.data.filePath;


        //var url = this.data.filePath + "&output=embed";
        //window.location.replace(url);

        iframe.onload = this.processFrame;
        let warning = document.createElement('h1');
        warning.align = "center";
        let message = document.createElement('p');
        event.path[1].appendChild(warning);
        event.path[1].appendChild(message);
        event.path[1].appendChild(iframe);
        setTimeout(function () {
            warning.innerHTML = 'Warning!'
            message.innerHTML = 'You don\'t have the plugin, needed to view this document';
        },200);

    }

    processFrame(event) {
        event.path[1].removeChild(event.path[1].children[0]);
        event.path[1].removeChild(event.path[1].children[0]);
        event.path[3].style.width = '600px';
        event.path[3].style.height = '600px';
        let iframe = event.path[1].children[0];
        iframe.style.align = 'center';
        iframe.style.width = '550px';
        iframe.style.height = '550px';
        iframe.style.opacity = '1';
    }

    changeUrl(url): SafeUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    show(event) {
        console.log(event);
    }

}
