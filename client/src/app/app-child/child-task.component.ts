import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '../_services/HttpClient';
import 'rxjs/Rx';
import { AppSettings } from '../app.settings';
import { CommentDialogComponent } from './comment-dialog/comment-dialog.component';
import { FileDialogComponent } from './file-dialog/file-dialog.component';
import { MdDatepicker, MdDialog } from "@angular/material";
import { Mark } from './mark.model';
import { File } from './file.model';

@Component({
    selector: 'app-child',
    templateUrl: 'child-task.component.html',
    styleUrls: ['./child-task.component.css']
})

export class ChildTaskComponent implements OnInit {
    @ViewChild('pickerStart') startDatePicker: MdDatepicker<Date>;
    @ViewChild('pickerEnd') endDatePicker: MdDatepicker<Date>;
    marks: Array<Mark[]>;
    marksByDate: Array<Mark[]>;
    startDate: Date;
    endDate: Date;
    http: HttpClient;

    constructor(private _http: HttpClient, public dialog: MdDialog) {
        this.http = _http;
    }

    ngOnInit() {
        this.currentWeek();
    }

    getUserId(): string{
      const jwt = localStorage.getItem('currentUser');
      const jwtData = jwt.split('.')[1];
      const decodedJwtJsonData = window.atob(jwtData);
      const decodedJwtData = JSON.parse(decodedJwtJsonData);
      return decodedJwtData.userid;
    }

    fetchTasksByDate(childId: string, criterias: string) {
      this.http.get(AppSettings.URL + AppSettings.childMarkReadByCriteria(childId, criterias)).subscribe(response => {
        let marks = response.json();
        console.log(marks.length);
        this.marks = this.createMarksByDate(marks);
      });


        /*this.http.get(AppSettings.URL + AppSettings.CHILD_TASKS + `?child-id=${childId}${criterias}`).subscribe(response => {
            let marks = response.json();
            console.log(marks.length);
            this.marks = this.createMarksByDate(marks);
        });*/
    }

    createMarksByDate(marks: Mark[]): Array<Mark[]> {
        const resultObject: Array<Mark[]> = [];
        for (const mark of marks) {
            if (resultObject[mark.date] === undefined) {
                resultObject[mark.date] = [];
            }
            resultObject[mark.date].push(mark);
            console.log(mark.comment);
        }
        return resultObject;
    }

    filterByDate() {
        if (
            (this.startDatePicker._selected !== null && this.startDatePicker._selected !== undefined) &&
            (this.endDatePicker._selected !== null && this.endDatePicker._selected !== undefined)
        ) {
            let startDate: string = ('0' + this.startDatePicker._selected.getDate()).slice(-2) + '.'
                + ('0' + (this.startDatePicker._selected.getMonth() + 1)).slice(-2) + '.' + this.startDatePicker._selected.getFullYear();
            let endDate: string = ('0' + this.endDatePicker._selected.getDate()).slice(-2) + '.'
                + ('0' + (this.endDatePicker._selected.getMonth() + 1)).slice(-2) + '.' + this.endDatePicker._selected.getFullYear();
            console.log('Search button clicked startDate = ' + startDate + ', endDate = ' + endDate);
            this.fetchTasksByDate(this.getUserId(), `&start-date=${startDate}&end-date=${endDate}`);
        }
    }

    previousWeek() {
        let now = this.startDatePicker._selected; // get current newDate
        now.setDate(now.getDate() - 1);
        let sunday = new Date(now);
        sunday.setDate(sunday.getDate() - sunday.getDay());
        console.log('ok -->> ' + (sunday.getDate() - sunday.getDay()));
        let saturday = new Date(sunday);
        saturday.setDate(saturday.getDate() + 6);
        this.startDatePicker._select(sunday);
        this.endDatePicker._select(saturday);
        this.filterByDate();
    }

    nextWeek() {
        let now = this.endDatePicker._selected; // get current newDate
        now.setDate(now.getDate() + 1);
        let sunday = new Date(now);
        sunday.setDate(sunday.getDate() - sunday.getDay());
        let saturday = new Date(now);
        saturday.setDate(sunday.getDate() + 6);
        this.startDatePicker._select(sunday);
        this.endDatePicker._select(saturday);
        this.filterByDate();
    }

    currentWeek() {
        let now = new Date();
        let sunday = new Date(now);
        sunday.setDate(sunday.getDate() - sunday.getDay());
        let saturday = new Date(now);
        saturday.setDate(saturday.getDate() + 6);
        this.startDatePicker._select(sunday);
        this.endDatePicker._select(saturday);
        this.filterByDate();
    }

    fetchDates(marks: Array<Mark[]>): string[] {
        const dates: string[] = [];
        for (let date in marks) {
            dates.push(date);
        }
        return dates;
    }

    clearDate() {
        this.startDate = null;
        this.endDate = null;
    }

    startDateChanged({ value }) {
        this.startDate = value;
    }

    endDateChanged({ value }) {
        this.endDate = value;
    }

    shortComment(comment: string) {
        if (comment.length < 20) {
            return comment;
        } else {
            return comment.substring(0, 20) + ' ...';
        }
    }

    openCommentDialog(message: string): void {
        console.log('openDialog() message = ' + message);
        const dialogRef = this.dialog.open(CommentDialogComponent, {
            height: '300px',
            width: '500px',
            data: { msg: message }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    openFileDialog(file: File) {
        let path: string = file.filePath;
        console.log('file path berfore dialog.open --> ' + path);
        const dialogRef = this.dialog.open(FileDialogComponent, {
            height: '200px',
            width: '300px',
            data: {
                filePath: path
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }
}
