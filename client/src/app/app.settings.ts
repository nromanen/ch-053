export class AppSettings {
  public static URL = 'http://localhost:8080/journal-1.0'; // todo rename to BASE-URL journal-1.0

  // SchoolClass
  public static SCHOOL_CLASS_READ_ALL = '/api/school-classes/teacher';
  public static SCHOOL_CLASS_CREATE = '/api/school-classes';

  public static schoolClassReadByTeacherId(teacherId): string {
    return this.SCHOOL_CLASS_READ_ALL + `?teacherId=${teacherId}`;
  }
  // Child
  public static CHILD_READ_ALL = '/api/children/parent';

  public static childReadBySchoolClassId(schoolClassId) {
    return this.CHILD_READ_ALL + `?schoolClassId=${schoolClassId}`;
  }
  public static childReadByParentId(parentId) {
    return this.CHILD_READ_ALL + `?parentId=${parentId}`;
  }

  //Teacher
  public static TEACHER_READ_NOT_CURATOR = '/api/teachers/free-teacher?findFreeOnly=true';

  //Lesson
  public static LESSON_READ_BY_TEACHER_AND_SCHOOLCLASS = '/api/lessons/teacherschoolclass';
  public static LESSON_READ_BY_TEACHER = '/api/lessons/teacher';
  public static lessonReadByTeacherIdBySchoolClassId(teacherId, schoolClassId) {
    return this.LESSON_READ_BY_TEACHER_AND_SCHOOLCLASS + `?teacherId=${teacherId}&schoolClassId=${schoolClassId}`;
  }

  public static lessonReadByTeacherId(teacherId): string {
      return this.LESSON_READ_BY_TEACHER + `?teacherId=${teacherId}`;
  }

  //ChildMark
  public static CHILD_MARK_READ_ALL = '/api/child-marks/date';
  public static CHILD_MARK_CREATE = '/api/child-marks';
  public static CHILD_MARK_UPDATE = '/api/child-marks/mark';
  public static LESSON_EVENT_TYPE_BY_CHILD_ID = '/api/child-marks/subjects';

  public static childMarkReadByCriteria(childId, criteria): string {
    return this.CHILD_MARK_READ_ALL + `?child-id=${childId}${criteria}`;
  }
  public static childrenMarkByLessonEventId(lessonEventId) {
    return this.CHILD_MARK_READ_ALL + `?lessonEventId=${lessonEventId}`;
  }

  public static childSubjectsById (childId): string {
      return this.LESSON_EVENT_TYPE_BY_CHILD_ID + `?child-id=${childId}`;
  }

  //LessonEventType
  public static LESSON_EVENT_TYPE_READ_ALL = '/api/lesson-event-types/type';

  //LessonEvent
  public static LESSON_EVENT_CREATE = '/api/lesson-events';
  public static LESSON_EVENT_UPDATE = '/api/lesson-events/event';
  public static LESSON_EVENT_READ = '/api/lesson-events/lessons';
  public  static lessonEventsByTeacher(teacherId) {
    return this.LESSON_EVENT_READ + `?teacherId=${teacherId}`
  }
  public static lessonEventFind(id) {
    return `/api/lesson-events/find?id=${id}`;
  }
}
