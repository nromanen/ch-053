import { Component, OnInit, Input, Inject } from '@angular/core';
import 'rxjs/Rx';
import { HttpClient } from "../../../_services/HttpClient";
import { MdDialog, MD_DIALOG_DATA, MdDialogRef } from "@angular/material";
import { Mark } from "../../child-table/marks-journal/mark.model";

@Component({
  selector: 'app-marks-by-subject',
  templateUrl: './marks-by-subject.component.html',
  styleUrls: ['./marks-by-subject.component.css']
})
export class MarksBySubjectComponent implements OnInit {
    
    childMarks: Mark[];
    subjects: {[key: string]: Mark[]} = {};
    
    @Input() childId: string;
    http: HttpClient;
    COMMENT_PREVIEW_LENGTH = 20;

  constructor( private _http: HttpClient, public dialog: MdDialog) { }
  
  getMarksOnSubject (subject: string): Mark[] {
          return this.subjects[subject];
  }
  
  getSetOfSubjects(): string[] {
          return Object.keys( this.subjects );
  }
  
  getMarksGroupedBySubject(): { [key: string]: Mark[] } {
      let setOfSubjects = new Set();
      let marksOnSubject: { [key: string]: Mark[] } = {};
      this.childMarks.forEach( mark => setOfSubjects.add( mark.subject ) );
      setOfSubjects.forEach( subject => marksOnSubject[subject] = []);
      this.childMarks.forEach( currentMark => marksOnSubject [currentMark.subject].push( currentMark ));
      return marksOnSubject;
  }
  
  shortComment( comment: string) {
      if ( comment.length < this.COMMENT_PREVIEW_LENGTH )
          return comment;
      else
          return comment.substring(0, this.COMMENT_PREVIEW_LENGTH) +  ' ...';
  }
  
  openDialog( message: string ): void {
      console.log( 'openDialog() message = ' + message );
      let dialogRef = this.dialog.open( DialogOverviewExampleDialogForSubject, {
          height: '300px',
          width: '500px',
          data: { msg: message}
      } );
      
      dialogRef.afterClosed().subscribe( result => {
         console.log( 'The dialog was closed' ); 
      });
  }
  
  initTableDataSource ( childMarks: Mark[] ) {
      this.childMarks = childMarks;
      this.subjects = this.getMarksGroupedBySubject();
  }

  ngOnInit() {
  }

}

@Component({
    selector: 'dialog-overview-example-dialog',
    templateUrl: 'dialog-overview-example-dialog.html',
} )
 export class DialogOverviewExampleDialogForSubject {
    
    name: string;

    constructor (
            public dialogRef: MdDialogRef<DialogOverviewExampleDialogForSubject>,
            @Inject( MD_DIALOG_DATA ) public data: any) {}
        
        onNoClick(): void {
            this.dialogRef.close()
        
    }
}

