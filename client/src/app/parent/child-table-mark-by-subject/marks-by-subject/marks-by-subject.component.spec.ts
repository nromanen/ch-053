import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarksBySubjectComponent } from './marks-by-subject.component';

describe('MarksBySubjectComponent', () => {
  let component: MarksBySubjectComponent;
  let fixture: ComponentFixture<MarksBySubjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarksBySubjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarksBySubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
