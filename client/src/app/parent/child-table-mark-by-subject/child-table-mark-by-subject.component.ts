import { Component, OnInit, Input, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { MarksBySubjectComponent } from "./marks-by-subject/marks-by-subject.component";
import { Child } from "../child-table/child.model";
import { HttpClient } from "../../_services/HttpClient";
import { MdIconRegistry, MdDatepicker, MdSelect } from "@angular/material";
import {DomSanitizer} from "@angular/platform-browser";
import { AppSettings } from "../../app.settings";
import 'rxjs/Rx';
import { MarksJournalComponent } from "../child-table/marks-journal/marks-journal.component";
import {Subject} from "./subject.model";



@Component({
  selector: 'app-child-table-mark-by-subject',
  templateUrl: './child-table-mark-by-subject.component.html',
  styleUrls: ['./child-table-mark-by-subject.component.css']
})
export class ChildTableMarkBySubjectComponent implements OnInit {
    
    @Input() SubjectSelect: String;
    @ViewChildren(MarksBySubjectComponent)
    marksJournals: QueryList<MarksBySubjectComponent>;
    allChildren: Child[];
    subjects: Map <String, Subject[]>;
    http: HttpClient;
    lessonEventTypes: String;
    subjectName: string; 
    initSubjectId: string;

    ngOnInit() {
        this.subjectName = sessionStorage.getItem("initSubject");
        this.subjects = new Map <String, Subject[]>();
        this.initData(this.getUserId());
    }
    
    initData(parentId: string) {
        let initSub;
        this.http.get(AppSettings.URL + AppSettings.childReadByParentId(parentId)).subscribe(response => {
            this.allChildren = response.json();
            this.allChildren.forEach(currentChild => {
            this.http.get(AppSettings.URL + AppSettings.childSubjectsById(currentChild.id)).subscribe(response => {
                this.subjects.set(currentChild.id, response.json());
                this.subjects.get(currentChild.id).forEach(subject => { 
                    if(subject.name === this.subjectName) { 
                        initSub = subject.id;
                    }
            this.allChildren.forEach(currentChild => {
                this.http.get(AppSettings.URL + AppSettings.childMarkReadByCriteria(currentChild.id, `&subject-id=${initSub}`))
                  .subscribe(childMarksResponse => {
                    currentChild.marks = childMarksResponse.json();
                    let currentMarksJournal: MarksBySubjectComponent =
                      this.marksJournals.find(journal => journal.childId === currentChild.id);
                    currentMarksJournal.initTableDataSource(currentChild.marks);
                          });
                        });
                     });
                });
            });
        });
    }
    
    constructor(private _http: HttpClient, iconRegistry: MdIconRegistry, sanitizer: DomSanitizer) {
      this.http = _http;
      this.http.get(AppSettings.URL + AppSettings.LESSON_EVENT_TYPE_READ_ALL).subscribe(response => this.lessonEventTypes = response.json());
    }

    getUserId(): string{
      const jwt = localStorage.getItem('currentUser');
      const jwtData = jwt.split('.')[1];
      const decodedJwtJsonData = window.atob(jwtData);
      const decodedJwtData = JSON.parse(decodedJwtJsonData);
      return decodedJwtData.userid;
    }
    
    onSelectSubject(childId: string, subjectId: string) {
        let currentChild: Child = this.allChildren.find(child => child.id === childId);
        this.http.get(AppSettings.URL + AppSettings.childMarkReadByCriteria(currentChild.id, `&subject-id=${subjectId}`))
        .subscribe(childMarksResponse => {
          currentChild.marks = childMarksResponse.json();
          let currentMarksJournal: MarksBySubjectComponent =
            this.marksJournals.find(journal => journal.childId === currentChild.id);
          currentMarksJournal.initTableDataSource(currentChild.marks);
      });
    }
    
}