import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildTableMarkBySubjectComponent } from './child-table-mark-by-subject.component';

describe('ChildTableMarkBySubjectComponent', () => {
  let component: ChildTableMarkBySubjectComponent;
  let fixture: ComponentFixture<ChildTableMarkBySubjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildTableMarkBySubjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildTableMarkBySubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
