export class Subject {
  private _id: string;
  private _name: string;
  private _alias: string;



  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get alias(): string {
    return this._alias;
  }

  set alias(value: string) {
    this._alias = value;
  }


  constructor(id: string, name: string, alias: string) {
    this._id = id;
    this._name = name;
    this._alias = alias;
    
  }
  
}
