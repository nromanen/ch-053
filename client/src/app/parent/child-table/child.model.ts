import {Mark} from './marks-journal/mark.model';
/**
 * Created by Aleks on 19.08.2017.
 */
export class Child {
  private _id: string;
  private _firstName: string;
  private _lastName: string;
  private _patronymic: string;
  private _marks: Mark[];


  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get patronymic(): string {
    return this._patronymic;
  }

  set patronymic(value: string) {
    this._patronymic = value;
  }

  get marks(): Mark[] {
    return this._marks;
  }

  set marks(value: Mark[]) {
    this._marks = value;
  }

  constructor(id: string, firstName: string, lastName: string, patronymic: string, marks: Mark[]) {
    this._id = id;
    this._firstName = firstName;
    this._lastName = lastName;
    this._patronymic = patronymic;
    this._marks = marks;
  }
}
