import { Component, Inject, Input, OnInit } from '@angular/core';
import { HttpClient } from '../../../_services/HttpClient';
import 'rxjs/Rx';
import { Mark } from './mark.model';
import { MD_DIALOG_DATA, MdDialogRef, MdDialog } from '@angular/material';

@Component( {
    selector: 'app-marks-journal',
    templateUrl: 'marks-journal.component.html',
    styleUrls: ['./marks-journal.component.css']
} )
export class MarksJournalComponent implements OnInit {

    childMarks: Mark[];
    dates: { [key: string]: Mark[] } = {};

    @Input() childId: string;
    http: HttpClient;
    COMMENT_PREVIEW_LENGTH = 20;
    subName: string;

    constructor( private _http: HttpClient, public dialog: MdDialog ) { }

    getMarksOnDate( date: string ): Mark[] {
        return this.dates[date];
    }

    getSetOfDates(): string[] {
        return Object.keys( this.dates );
    }

    getMarksGroupedByDate(): { [key: string]: Mark[] } {
        let setOfDates = new Set();
        let marksOnDate: { [key: string]: Mark[] } = {};
        this.childMarks.forEach( mark => setOfDates.add( mark.date ) );
        setOfDates.forEach( date => marksOnDate[date] = [] );
        this.childMarks.forEach( currentMark => marksOnDate[currentMark.date].push( currentMark ) );
        return marksOnDate;
    }


    shortComment( comment: string ) {
        if ( comment.length < this.COMMENT_PREVIEW_LENGTH )
            return comment;
        else
            return comment.substring( 0, this.COMMENT_PREVIEW_LENGTH ) + ' ...';
    }

    openDialog( message: string ): void {
        console.log( 'openDialog() message = ' + message );
        let dialogRef = this.dialog.open( DialogOverviewExampleDialog, {
            height: '300px',
            width: '500px',
            data: { msg: message }
        } );

        dialogRef.afterClosed().subscribe( result => {
            console.log( 'The dialog was closed' );
        } );
    }

    initTableDataSource( childMarks: Mark[] ) {
        this.childMarks = childMarks;
        this.dates = this.getMarksGroupedByDate();
    }
    
    onSubjectClick (subjectName: string) {
        this.subName = subjectName;
        sessionStorage.setItem("initSubject", this.subName);
    }

    ngOnInit() { }

}

@Component( {
    selector: 'dialog-overview-example-dialog',
    templateUrl: 'dialog-overview-example-dialog.html',
} )
export class DialogOverviewExampleDialog {

    name: string;

    constructor(
        public dialogRef: MdDialogRef<DialogOverviewExampleDialog>,
        @Inject( MD_DIALOG_DATA ) public data: any ) { }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
