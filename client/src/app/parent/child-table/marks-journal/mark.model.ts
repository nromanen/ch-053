/**
 * Created by Aleks on 28.08.2017.
 */
export class Mark {
  id: string;
  date: string;
  mark: string;
  subject: string;
  lessonEventTypeName: string;
  comment: string;

  constructor(id: string, date: string, mark: string, subject: string, lessonEventTypeName: string, comment: string) {
    this.id = id;
    this.date = date;
    this.mark = mark;
    this.subject = subject;
    this.lessonEventTypeName = lessonEventTypeName;
    this.comment = comment;
  }

  getShortComment(): string{
    return 'sadfasd';
  }

}
