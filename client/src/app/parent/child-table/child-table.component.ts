import {Component, OnInit, QueryList, ViewChildren, Input, ViewChild} from '@angular/core';
import {Child} from './child.model';
import 'rxjs/Rx';
import {AppSettings} from "../../app.settings";
import {MarksJournalComponent} from "./marks-journal/marks-journal.component";
import {MdIconRegistry, MdDatepicker, PageEvent} from "@angular/material";
import {DomSanitizer} from "@angular/platform-browser";
import {HttpClient} from "../../_services/HttpClient";

@Component({
  selector: 'app-child-table',
  templateUrl: 'child-table.component.html',
  styleUrls: ['./child-table.component.css']
})

export class ChildTableComponent implements OnInit {

  @Input() pickerEnd: Date;
  @ViewChild('pickerStart') startDatePicker: MdDatepicker<Date>;
  @ViewChild('pickerEnd') endDatePicker: MdDatepicker<Date>;
  @ViewChildren(MarksJournalComponent)
  marksJournals: QueryList<MarksJournalComponent>;
  allChildren: Child[];
  http: HttpClient;
  startDate: Date;
  endDate: Date;


  ngOnInit() {
    this.currentWeek();
  }

  fetchAllChildren(parentId: string, criterias: string){
    this.http.get(AppSettings.URL + AppSettings.childReadByParentId(parentId)).subscribe(response => {
      this.allChildren = response.json();
      this.allChildren.forEach(currentChild => {
        this.http.get(AppSettings.URL + AppSettings.childMarkReadByCriteria(currentChild.id, criterias))
          .subscribe(childMarksResponse => {
            currentChild.marks = childMarksResponse.json();
            let currentMarksJournal: MarksJournalComponent =
              this.marksJournals.find(journal => journal.childId === currentChild.id);
            currentMarksJournal.initTableDataSource(currentChild.marks);
        });
      })
    });
  }

  constructor(private _http: HttpClient, iconRegistry: MdIconRegistry, sanitizer: DomSanitizer) {
    this.http = _http;
  }

  getUserId(): string{
    const jwt = localStorage.getItem('currentUser');
    const jwtData = jwt.split('.')[1];
    const decodedJwtJsonData = window.atob(jwtData);
    const decodedJwtData = JSON.parse(decodedJwtJsonData);
    return decodedJwtData.userid;
  }

  filterByDate(){
    if(
      (this.startDatePicker._selected !== null && this.startDatePicker._selected !== undefined) &&
      (this.endDatePicker._selected !== null && this.endDatePicker._selected !== undefined)
    ){
      let startDate: string = ('0' + this.startDatePicker._selected.getDate()).slice(-2) + '.'
        + ('0' + (this.startDatePicker._selected.getMonth()+1)).slice(-2) + '.' + this.startDatePicker._selected.getFullYear();
      let endDate: string = ('0' + this.endDatePicker._selected.getDate()).slice(-2) + '.'
        + ('0' + (this.endDatePicker._selected.getMonth()+1)).slice(-2) + '.' + this.endDatePicker._selected.getFullYear();
      console.log('Search button clicked startDate = ' + startDate + ', endDate = ' + endDate);
      this.fetchAllChildren(this.getUserId(), `&start-date=${startDate}&end-date=${endDate}`);
    }
  }

  previousWeek(){
    let now = this.startDatePicker._selected; // get current newDate
    now.setHours(0,0,0,0);
    now.setDate(now.getDate() - 2);

    // Get the previous Monday
    let monday = new Date(now);
    monday.setDate(monday.getDate() - monday.getDay() + 1);

    // Get next Sunday
    let sunday = new Date(now);
    sunday.setDate(sunday.getDate() - sunday.getDay() + 7);

    this.startDate = monday;
    this.endDate = sunday;
    this.startDatePicker._select(monday);
    this.endDatePicker._select(sunday);

    this.filterByDate();
  }

  nextWeek(){
    let now = this.endDatePicker._selected; // get current newDate
    now.setHours(0,0,0,0);
    now.setDate(now.getDate() + 1);

    // Get the previous Monday
    let monday = new Date(now);
    monday.setDate(monday.getDate() - monday.getDay() + 1);

    // Get next Sunday
    let sunday = new Date(now);
    sunday.setDate(sunday.getDate() - sunday.getDay() + 7);

    this.startDate = monday;
    this.endDate = sunday;
    this.startDatePicker._select(monday);
    this.endDatePicker._select(sunday);
    this.filterByDate();
  }

  currentWeek(){
    let now = new Date(); // get current newDate
    now.setHours(0,0,0,0);
    console.log('today = ' + now);

    // Get the previous Monday
    let monday = new Date(now);
    monday.setDate(monday.getDate() - monday.getDay() + 1);

    // Get next Sunday
    let sunday = new Date(now);
    sunday.setDate(sunday.getDate() - sunday.getDay() + 7);

    this.startDate = monday;
    this.endDate = sunday;
    this.startDatePicker._select(monday);
    this.endDatePicker._select(sunday);
    this.filterByDate();
  }

  clearDate(){
    this.currentWeek();
  }
}
