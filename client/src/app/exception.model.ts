export class Exception {
  id: string;
  english: string;
  ukrainian: string;
  public static exceptions: Exception[] = [new Exception('no_format_school_class_name', '', ''), new Exception('school_class_already_exist', '', '')];

  constructor(id: string, english: string, ukrainian: string) {
    this.id;
    this.english = english;
    this.ukrainian = ukrainian;
  }
}
