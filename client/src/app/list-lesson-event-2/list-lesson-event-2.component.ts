import { Component, OnInit } from '@angular/core';
import { ViewChild} from '@angular/core';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import {MdPaginator} from "@angular/material";
import {AppSettings} from "../app.settings";
import {HttpClient} from "../_services/HttpClient";
import {Router} from "@angular/router";
import {element} from "protractor";

@Component({
  selector: 'app-list-lesson-event-2',
  templateUrl: './list-lesson-event-2.component.html',
  styleUrls: ['./list-lesson-event-2.component.css']
})
export class ListLessonEvent2Component implements OnInit {
  displayedColumns = ['schoolClassName', 'subjectName', 'eventDate', 'openLessonEvent'];

  http: HttpClient;
  exampleDatabase;
  dataSource: ExampleDataSource | null;
  disabledCompleted: boolean = false;
  disabledNotFinished: boolean = false;
  constructor(http: HttpClient, private router: Router) {
    this.http = http;
    this.exampleDatabase = new ExampleDatabase(this.http);
  }

  @ViewChild(MdPaginator) paginator: MdPaginator;

  ngOnInit() {
    this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, 'all');
  }

  updateLessonEvent(id) {
    this.router.navigate(['/update-lesson-event'], { queryParams: { id: id } });
  }
  notCompleted() {
    if (!this.disabledNotFinished) {
      if (this.disabledCompleted) {
        this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, 'all');
      }
      else {
        this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, 'notCompleted');
      }
    }
  }
  completed() {
    if (!this.disabledCompleted) {
      if (this.disabledNotFinished) {
        this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, 'all');
      }
      else {
        this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, 'completed');
      }
    }
  }
}

export class Element {
/*  number: number = 0;*/
  id: string;
  eventDate: string;
  schoolClassName: string;
  subjectName: string;
  completed: boolean;
}

export class ExampleDatabase {
  dataChange: BehaviorSubject<Element[]> = new BehaviorSubject<Element[]>([]);
  get data(): Element[] { return this.dataChange.value; }
  http: HttpClient;
  teacherId = this.getUserId();

  constructor(http: HttpClient) {
    this.http = http;
    this.http.get(AppSettings.URL + AppSettings.lessonEventsByTeacher(this.teacherId)).subscribe(response => {
      this.dataChange.next(response.json())});
  }

  getUserId(): string {
    const jwt = localStorage.getItem('currentUser');
    const jwtData = jwt.split('.')[1];
    const decodedJwtJsonData = window.atob(jwtData);
    const decodedJwtData = JSON.parse(decodedJwtJsonData);
    return decodedJwtData.userid;
  }
}

export class ExampleDataSource extends DataSource<any> {
  constructor(private _exampleDatabase: ExampleDatabase, private _paginator: MdPaginator, private isCompleted: string) {
    super();
  }
  connect(): Observable<Element[]> {
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._paginator.page,
    ];

    return Observable.merge(...displayDataChanges).map(() => {
      let data;
      if (this.isCompleted === 'all') {
        data = this._exampleDatabase.data.slice();
      }
      if (this.isCompleted === 'notCompleted') {
        data = this._exampleDatabase.data.slice().filter(childMark => childMark.completed === false);
      }
      if (this.isCompleted === 'completed') {
        data = this._exampleDatabase.data.slice().filter(childMark => childMark.completed === true);
      }
      this._paginator.length = data.length;
      // Grab the page's slice of data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      return data.splice(startIndex, this._paginator.pageSize);
    });
  }

  disconnect() {}
}
