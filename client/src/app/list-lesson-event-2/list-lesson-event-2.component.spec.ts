import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLessonEvent2Component } from './list-lesson-event-2.component';

describe('ListLessonEvent2Component', () => {
  let component: ListLessonEvent2Component;
  let fixture: ComponentFixture<ListLessonEvent2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListLessonEvent2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLessonEvent2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
