import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from "@angular/material";
import {DialogOverviewExampleDialog} from "../parent/child-table/marks-journal/marks-journal.component";
import {Router, ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-dialog-yes-or-no',
  templateUrl: './dialog-yes-or-no.component.html',
  styleUrls: ['./dialog-yes-or-no.component.css']
})
export class DialogYesOrNoComponent implements OnInit {

  constructor(@Inject(MD_DIALOG_DATA) public data: any, public dialogRef: MdDialogRef<DialogOverviewExampleDialog>, private _router: Router) {
  }

  ngOnInit() {
  }
  yes() {
    this._router.navigate(['/list-lesson-event-2']);
  }
  no() {
    this.dialogRef.close();
  }
}
