import {Component, OnInit} from '@angular/core';
import {AppSettings} from '../app.settings';
import {Lesson} from './lesson-event.model';
import {HttpClient} from '../_services/HttpClient';
import {AppService} from "../app.service";
import {DialogWarningComponent} from "../admin/school-class-table-2/add-school-class-dialog/dialog-warning/dialog-warning.component";
import {DateAdapter, MdDialog} from "@angular/material";
import {ChildByClassTableComponent, ChildClassServer} from "./child-by-class-table/child-by-class-table.component";
import {ninvoke} from "q";
import {FormControl, Validators} from "@angular/forms";
import {DialogComponent} from "../admin/school-class-table-2/add-school-class-dialog/dialog-succes/dialog.component";
import {DialogYesOrNoComponent} from "../dialog-yes-or-no/dialog-yes-or-no.component";

const DATA_PICKER = '[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]';

@Component({
  selector: 'app-lesson-event',
  templateUrl: './lesson-event.component.html',
  styleUrls: ['./lesson-event.component.css']
})
export class LessonEventComponent implements OnInit {
  dataPicker = new FormControl('', [
    Validators.required,
    Validators.pattern(DATA_PICKER)]);
  lessons;
  schoolClasses;
  lessonEventTypes;
  teacherId = this.getUserId();
  schoolClassId;
  lessonEventTypesId;
  selectSchoolClass = true;
  selectSubject = false;
  selectDate = false;
  selectTypeOfLesson = false;
  selectCommentAndButtons = false;
  showStudentsBySchoolClass = false;
  lessonId;
  comment;
  lessonEventId;
  startDate = new Date();
  date: Date;
  eventDateDay: string;
  eventDate: string;
  static lessonEvent;

  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    // Prevent Saturday and Sunday from being selected.
    return this.startDate.getTime() <= d.getTime() || d.toDateString() === this.startDate.toDateString();
    //return d.getUTCDate() >= this.startDate.getUTCDate();
  };
  static s: number;
  private http: HttpClient;

  constructor(http: HttpClient, private service: AppService, public dialog: MdDialog) {
    //this.http.get(AppSettings.URL + AppSettings.lessonEventsByTeacher(this.teacherId)).subscribe(response => this.listLessonEvents = response.json());
    this.date = new Date;
    console.log(this.date);
    this.http = http;
    this.http.get(AppSettings.URL + AppSettings.schoolClassReadByTeacherId(this.teacherId)).subscribe(response => this.schoolClasses = response.json());
    this.http.get(AppSettings.URL + AppSettings.LESSON_EVENT_TYPE_READ_ALL).subscribe(response => this.lessonEventTypes = response.json());
  }

  ngOnInit() {
  }

  getUserId(): string {
    const jwt = localStorage.getItem('currentUser');
    const jwtData = jwt.split('.')[1];
    const decodedJwtJsonData = window.atob(jwtData);
    const decodedJwtData = JSON.parse(decodedJwtJsonData);
    return decodedJwtData.userid;
  }

  onSelectSchoolClass(schoolClassId: number) {
    this.selectSubject = true;
    this.schoolClassId = schoolClassId;
    console.log(this.teacherId);
    this.http.get(AppSettings.URL + AppSettings.lessonReadByTeacherIdBySchoolClassId(this.teacherId, this.schoolClassId)).subscribe(response => this.lessons = response.json());
  }

  onSelectSubject(lessonId) {
    this.selectDate = true;
    this.selectSchoolClass = false;
    this.lessonId = lessonId;
  }

  onSelectDate(date, event) {
    console.log('ha-ha-ha');
    console.log(event);
    this.dataPicker.setValue(date);
    console.log(this.dataPicker.valid);
    //if (this.dataPicker.valid) {
    if (this.selectTypeOfLesson === true) {
      this.selectCommentAndButtons = true;
      this.selectDate = false;
    }
      this.selectTypeOfLesson = true;
      this.selectSubject = false;
    //}
  }

  onClickInputDate(date) {
    this.dataPicker.setValue(date);
    if (this.dataPicker.valid) {
      if (this.selectTypeOfLesson === true) {
        this.selectCommentAndButtons = true;
        this.selectDate = false;
      }
    this.selectTypeOfLesson = true;
    this.selectSubject = false;
    }
  }

  onSelectTypeOfLesson(lessonEventTypesId, date) {
    console.log(date);
    this.lessonEventTypesId = lessonEventTypesId;
    this.dataPicker.setValue(date);
    if (this.dataPicker.valid) {
      this.selectCommentAndButtons = true;
      this.selectDate = false;
    }
    else {
      this.dialog.open(DialogWarningComponent, {width: '250px', data: 'Please input valid value of date'});

    }
  }

  startLesson(date: string, comment: string) {
    let newDate = new Date(this.date);
    this.eventDateDay = newDate.getDate().toString();
    if (this.eventDateDay.length === 1) {
      this.eventDateDay = '0' + this.eventDateDay;
    }
    this.eventDate = newDate.getUTCMonth() + 1 + '/' + this.eventDateDay + '/' + newDate.getUTCFullYear();
    this.service.postJSON(new Lesson(null, this.lessonId, this.eventDate, comment, this.lessonEventTypesId, 'false', null), AppSettings.LESSON_EVENT_CREATE).subscribe(
      object => {
        ChildByClassTableComponent.lessonEventId = object.id;
        console.log(object.id);
        this.showStudentsBySchoolClass = true
      });
    this.selectTypeOfLesson = false;
    this.selectCommentAndButtons = false;
    /*this.showStudentsBySchoolClass = true;*/
    ChildByClassTableComponent.schoolClassId = this.schoolClassId;
    ChildByClassTableComponent.lessonEventTypeId = this.lessonEventTypesId;
    ChildByClassTableComponent.lessonId = this.lessonId;
    ChildByClassTableComponent.comment = this.comment;
    ChildByClassTableComponent.date = this.eventDate;
    console.log(date);
    console.log(comment);
  }
  goBack() {
    this.dialog.open(DialogYesOrNoComponent, {width: '250px', data: 'Are you sure?'});
  }
}
