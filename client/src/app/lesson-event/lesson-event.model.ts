export class Lesson {
  constructor(public id: string,
              public lessonId: string,
              public eventDate: string,
              public comment,
              public lessonEventTypeId: string,
              public completed: string,
              public completionDate: string)
  { }
}
