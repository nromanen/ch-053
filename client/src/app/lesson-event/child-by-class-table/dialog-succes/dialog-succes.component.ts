import {Component, Inject, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {MD_DIALOG_DATA} from "@angular/material";

@Component({
  selector: 'app-dialog-succes',
  templateUrl: './dialog-succes.component.html',
  styleUrls: ['./dialog-succes.component.css']
})
export class DialogSuccesComponent {

  public _router: Router;
  constructor(@Inject(MD_DIALOG_DATA) public data: any, _router: Router) {
    this._router = _router;
  }
}
