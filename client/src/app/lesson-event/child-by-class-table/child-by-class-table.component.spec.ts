import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildByClassTableComponent } from './child-by-class-table.component';

describe('ChildByClassTableComponent', () => {
  let component: ChildByClassTableComponent;
  let fixture: ComponentFixture<ChildByClassTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildByClassTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildByClassTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
