export class ChildMarkModel {
  firstName: string;
  lastName: string;
  id: string;
  date: string;
  absent: string;
  mark: string;
  subject: string;
  comment: string;
  childId: string;
  files: string[];
  lessonEventTypeName: string;
  lessonId: string;
  constructor(firstName: string, lastName: string, id: string, date: string, absent: string, mark: string, subject: string, comment: string, childId: string, files: string[], lessonEventTypeName: string, lessonId: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.id = id;
    this.date = date;
    this.absent = absent;
    this.mark = mark;
    this.subject = subject;
    this.comment = comment;
    this.childId = childId;
    this.files = files;
    this.lessonEventTypeName = lessonEventTypeName;
    this.lessonId = lessonId;
  }
}
