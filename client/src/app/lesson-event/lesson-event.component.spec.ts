import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonEventComponent } from './lesson-event.component';

describe('LessonEventComponent', () => {
  let component: LessonEventComponent;
  let fixture: ComponentFixture<LessonEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
