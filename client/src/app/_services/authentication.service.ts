﻿import {AppSettings} from "../app.settings";

﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';



@Injectable()
export class AuthenticationService {



    constructor(private http: Http) { }

    login(username: string, password: string) {
      const headers = new Headers ({'Content-Type': 'application/json'});
      const options = new RequestOptions ({headers: headers});
        return this.http.post(AppSettings.URL + '/auth', JSON.stringify({ username: username, password: password }), options)
             .map((response: Response) => {
                // login successful if there's a jwt token in the response
                const user = response.json();
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');

    }


}
