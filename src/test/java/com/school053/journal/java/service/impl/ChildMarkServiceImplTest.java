package com.school053.journal.java.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.school053.journal.java.H2WebConfiguration;
import com.school053.journal.java.configuration.AppConfig;
import com.school053.journal.java.configuration.WebConfiguration;
import com.school053.journal.java.dto.ChildMarkDto;
import com.school053.journal.java.service.ChildMarkService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class, H2WebConfiguration.class, WebConfiguration.class })
@WebAppConfiguration
public class ChildMarkServiceImplTest {

	@Autowired
	ChildMarkService childMarkService;

	@Test
	public void testChildMarkServiceCreation() {
		assertNotNull(childMarkService);
	}

	@Test
	public void testReadByChildId() {
		Map<String, Object> criteria = new HashMap<>();
		criteria.put("childId", "204");
		List<ChildMarkDto> childMarkDtos = childMarkService.read(criteria);
		int expected = 29;
		int actual = childMarkDtos.size();
		assertEquals(expected, actual);
		criteria.put("childId", "205");
		expected = 31;
		actual = childMarkService.read(criteria).size();
		assertEquals(expected, actual);
	}
}
