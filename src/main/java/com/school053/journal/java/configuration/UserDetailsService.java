package com.school053.journal.java.configuration;


import com.school053.journal.java.dao.UserDao;
import com.school053.journal.java.jwt.JwtUserFactory;
import com.school053.journal.java.model.security.Authority;
import com.school053.journal.java.model.security.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
@Transactional(readOnly = true)
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
	
	@Autowired
	private UserDao userdao;

    @Autowired
    public static org.springframework.security.core.userdetails.User getPrincipal() {
        return (org.springframework.security.core.userdetails.User) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
    }

    public static boolean isAuthorized() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return !(authentication instanceof AnonymousAuthenticationToken);
    }

    public static boolean hasAuthority(Authority.AuthorityType authorityType) {
        return getPrincipal().getAuthorities()
                .stream()
                .anyMatch(authority -> authority.getAuthority().equals(authorityType.name()));
    }

    public static List<GrantedAuthority> getAuthority() {
        return new ArrayList<>(getPrincipal().getAuthorities());
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	User user;
    	try {
    		user = userdao.findUserByUsername(username);
    	} catch (Exception e) {
    		
    		throw new UsernameNotFoundException("Could not load user!");
    	}
    	if (user == null) {
    		throw new UsernameNotFoundException("User not found!");
    	}
    
        return JwtUserFactory.create(user);
    }
}