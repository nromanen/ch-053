package com.school053.journal.java.configuration;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import springfox.documentation.spring.web.json.Json;

public class SpringfoxJsontoGsonAdapter implements JsonSerializer<Json> {

	@Override
	public JsonElement serialize(Json json, Type type, JsonSerializationContext context) {
		final JsonParser parser = new JsonParser();
		return parser.parse(json.value());
	}
}
