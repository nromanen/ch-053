package com.school053.journal.java.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/")
public class PageController {

    @GetMapping(value = {"","children", "lesson-event", "login", "school-classes", "tasks", "marksBySubject", "list-lesson-event-2", "update-lesson-event"} )
    public String getHomePage() {
        return "index";
    }
}
