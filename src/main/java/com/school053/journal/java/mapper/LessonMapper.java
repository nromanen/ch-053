package com.school053.journal.java.mapper;

import com.school053.journal.java.dto.LessonDto;
import com.school053.journal.java.model.events.Lesson;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LessonMapper {
    LessonMapper MAPPER = Mappers.getMapper(LessonMapper.class);

    @Mappings({
            @Mapping(target = "id", source = "lesson.id"),
            @Mapping(target = "schoolClassId", source = "lesson.schoolClass.id"),
            @Mapping(target = "subjectId", source = "lesson.subject.id"),
            @Mapping(target = "teacherId", source = "teacher.id"),
            @Mapping(target = "schoolClassName", source = "lesson.schoolClass.name"),
            @Mapping(target = "subjectName", source = "lesson.subject.name")
    })
    LessonDto toDto(Lesson lesson);
    
    
}
