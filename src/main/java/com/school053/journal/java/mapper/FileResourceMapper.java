package com.school053.journal.java.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.school053.journal.java.dto.FileResourceDto;
import com.school053.journal.java.model.events.FileResource;

@Mapper
public interface FileResourceMapper {

	@Mappings({ @Mapping(target = "id", source = "fileResource.id"),
			@Mapping(target = "fileName", source = "fileResource.name"),
			@Mapping(target = "filePath", source = "fileResource.relativePath") })

	FileResourceDto toDto(FileResource fileResource);
}
