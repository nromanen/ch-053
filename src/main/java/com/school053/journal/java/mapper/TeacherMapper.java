package com.school053.journal.java.mapper;

import com.school053.journal.java.dto.TeacherDto;
import com.school053.journal.java.model.users.Teacher;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TeacherMapper {
    TeacherMapper MAPPER = Mappers.getMapper(TeacherMapper.class);

    TeacherDto toDto(Teacher teacher);

    @InheritInverseConfiguration
    Teacher fromDto(TeacherDto teacherDto);
}
