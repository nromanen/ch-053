package com.school053.journal.java.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.school053.journal.java.dto.FileResourceDto;
import com.school053.journal.java.model.events.FileResource;

@Mapper(uses = FileResourceMapper.class)
public interface FileResourceListMapper {
	
	List<FileResourceDto> toDtos (List<FileResource> fileResources);

}
