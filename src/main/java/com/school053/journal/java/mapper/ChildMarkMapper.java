package com.school053.journal.java.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.school053.journal.java.dto.ChildMarkDto;
import com.school053.journal.java.model.events.ChildMark;
import org.mapstruct.factory.Mappers;

@Mapper(uses = FileResourceListMapper.class)
public interface ChildMarkMapper {
	ChildMarkMapper MAPPER = Mappers.getMapper(ChildMarkMapper.class);

	@Mappings({
			@Mapping(target = "id", source = "childMark.id"),
			@Mapping(target = "childId", source = "childMark.child.id"),
			@Mapping(target = "absent", source = "childMark.absent"),
			@Mapping(target = "mark", source = "childMark.mark"),
			@Mapping(target = "lessonEventId", source = "lessonEvent.id"),
			@Mapping(target = "lessonEventTypeId", source = "lessonEventType.id"),
			@Mapping(target = "lastName", source = "child.lastName"),
			@Mapping(target = "firstName", source = "child.firstName"),
			@Mapping(target = "date", source = "childMark.lessonEvent.eventDate", dateFormat = "dd.MM.yyyy"),

			@Mapping(target = "subject", source = "childMark.lessonEvent.lesson.subject.name"),
			@Mapping (target = "lessonEventTypeName", source = "childMark.lessonEventType.name"),
			@Mapping(target = "comment", source = "childMark.lessonEvent.comment"),
			@Mapping (target = "fileResourcesDto", source = "childMark.lessonEvent.files"),
	})
	ChildMarkDto toDto(ChildMark childMark);

	@Mappings({
			@Mapping(target = "id", source = "id"),
			@Mapping(target = "child.id", source = "childId"),
			@Mapping(target = "absent", source = "absent"),
			@Mapping(target = "mark", source = "mark"),
			@Mapping(target = "lessonEvent.id", source = "lessonEventId"),
			@Mapping(target = "lessonEventType.id", source = "lessonEventTypeId"),
			/*@Mapping (target = "lessonEventType.name", source = "lessonEventTypeName"),*/
			@Mapping(target = "child.lastName", source = "lastName"),
			@Mapping(target = "child.firstName", source = "firstName")
	})
	ChildMark fromDto(ChildMarkDto childMarkDto);
}
