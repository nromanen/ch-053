package com.school053.journal.java.mapper;

import com.school053.journal.java.dto.SubjectDto;
import com.school053.journal.java.model.events.Subject;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper(uses = FileResourceListMapper.class)
public interface SubjectMapper {
    SubjectMapper MAPPER = Mappers.getMapper(SubjectMapper.class);
    
    @Mappings({
    	@Mapping(target = "id", source = "subject.id"),
    	@Mapping(target = "name", source = "subject.name"),
    })
    SubjectDto toDto(Subject subject);

    @InheritInverseConfiguration
    Subject fromDto(SubjectDto subjectDto);
}
