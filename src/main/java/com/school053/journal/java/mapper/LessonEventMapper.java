package com.school053.journal.java.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.school053.journal.java.dto.LessonEventDto;
import com.school053.journal.java.model.events.LessonEvent;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LessonEventMapper {
	LessonEventMapper MAPPER = Mappers.getMapper(LessonEventMapper.class);

	@Mappings({ @Mapping(target = "id", source = "lessonEvent.id"),
			@Mapping(target = "comment", source = "lessonEvent.comment"),
			@Mapping(target = "eventDate", source = "lessonEvent.eventDate",	dateFormat = "M/dd/yyyy"),
			@Mapping(target = "lessonId", source = "lessonEvent.lesson.id"),
			@Mapping(target = "completed", source = "lessonEvent.completed"),
			@Mapping(target = "completionDate", source = "lessonEvent.completionDate", dateFormat = "M/dd/yyyy"),
			@Mapping(target = "subjectName", source = "lessonEvent.lesson.subject.name"),
			@Mapping(target = "schoolClassName", source = "lessonEvent.lesson.schoolClass.name")
			/*@Mapping(target = "lessonEventTypeId", source = "lessonEventTypeName.id")*/})
	LessonEventDto toDto(LessonEvent lessonEvent);

	@InheritInverseConfiguration
	LessonEvent fromDto(LessonEventDto lessonEventDto);
}
