package com.school053.journal.java.model.upload;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
 
@Component
public class FileWithInfoValidator implements Validator {
     
    public boolean supports(Class<?> clazz) {
        return FileWithInfo.class.isAssignableFrom(clazz);
    }
 
    public void validate(Object obj, Errors errors) {
    	FileWithInfo file = (FileWithInfo) obj;
         
        if(file.getFile()!=null){
            if (file.getFile().getSize() == 0) {
                errors.rejectValue("file", "missing.file");
            }
        }
    }
}