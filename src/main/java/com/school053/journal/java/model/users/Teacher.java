package com.school053.journal.java.model.users;


import com.school053.journal.java.model.security.User;

import javax.persistence.*;

@Entity
@Table(name="teachers")
@NamedQueries(
        @NamedQuery(name = Teacher.FIND_NOT_CURATOR, query = "FROM Teacher t where t.id not in (select curator.id from SchoolClass)")
)
public class Teacher extends User {

    public final static String FIND_NOT_CURATOR = "Teacher.fetchTeacherNotCurator";

    @Column(name = "description")
    private String description;

    public Teacher() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Teacher teacher = (Teacher) o;

        return description != null ? description.equals(teacher.description) : teacher.description == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

}
