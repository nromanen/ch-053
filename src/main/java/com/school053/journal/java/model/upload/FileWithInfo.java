package com.school053.journal.java.model.upload;

import org.springframework.web.multipart.MultipartFile;
 
public class FileWithInfo {
 
	String info;
	
    MultipartFile file;
    
    public String getInfo() {
    	return info;
    }
    
    public void setInfo(String info) {
    	this.info = info;
    }
     
    public MultipartFile getFile() {
        return file;
    }
 
    public void setFile(MultipartFile file) {
        this.file = file;
    }
}