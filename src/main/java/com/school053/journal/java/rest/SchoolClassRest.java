package com.school053.journal.java.rest;

import com.school053.journal.java.constant.Param;
import com.school053.journal.java.dto.LessonDto;
import com.school053.journal.java.dto.SchoolClassDto;
import com.school053.journal.java.model.users.SchoolClass;
import com.school053.journal.java.service.SchoolClassService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/school-classes")
public class SchoolClassRest {
    private final SchoolClassService schoolClassService;

    @Autowired
    public SchoolClassRest(SchoolClassService schoolClassService) {
        this.schoolClassService = schoolClassService;
    }

    //create
    @ApiOperation(value = "Create school class", response = ResponseEntity.class)
    @PostMapping
    public ResponseEntity create(@RequestBody SchoolClassDto schoolClassDto){
        try {
            SchoolClass schoolClass = schoolClassService.create(schoolClassDto);
            return ResponseEntity.ok(schoolClass);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //read
    @ApiOperation(value = "Get school classes by teacher id", notes = "if add new criteria, can use this map and add criteria in hibernate", response = SchoolClassDto.class)
    @GetMapping(value = "/teacher")
    public ResponseEntity<List<SchoolClassDto>> read(
            @RequestParam(value = Param.teacherId, required = false) String teacherId) {
        Map<String, Object> criteria = new HashMap<>();
        criteria.put(Param.teacherId, teacherId);
        return ResponseEntity.ok(schoolClassService.read(criteria));
    }

    //update

    //delete
}
