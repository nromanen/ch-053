package com.school053.journal.java.rest;

import com.school053.journal.java.constant.Param;
import com.school053.journal.java.dto.ChildDto;
import com.school053.journal.java.dto.LessonEventDto;
import com.school053.journal.java.service.ChildService;
import com.school053.journal.java.service.LessonEventService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/children")
public class ChildRest {
    private final ChildService childService;
    LessonEventDto lessonEventDto = new LessonEventDto();

    @Autowired
    public ChildRest(ChildService childService) {
        this.childService = childService;
    }

    //create

    //read
    @ApiOperation(value = "Get children by criteria(parent id, school class id)", response = ChildDto.class)
    @GetMapping(value = "/parent")
    public ResponseEntity<List<ChildDto>> searchChildren(
        @RequestParam(value = Param.readChildByParentId, required = false) String parentId,
        @RequestParam(value = Param.schoolClassId, required = false) String schoolClassId
    ) {
        Map<String, Object> criteria = new HashMap<>();
        criteria.put(Param.readChildByParentId, parentId);
        criteria.put(Param.schoolClassId, schoolClassId);
        return ResponseEntity.ok(childService.read(criteria));
    }

    //update

    //delete
}
