package com.school053.journal.java.rest;

import com.school053.journal.java.dto.LessonDto;
import com.school053.journal.java.dto.SchoolClassDto;
import com.school053.journal.java.service.LessonService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/lessons")
public class LessonRest {
    private final LessonService lessonService;

    @Autowired
    public LessonRest(LessonService lessonService) {
        this.lessonService = lessonService;
    }

    //create

    // old version by Igor by criteria
    @ApiOperation(value = "Get lessons by criteria", response = LessonDto.class)
    @GetMapping(value = "/teacherschoolclass")
    public ResponseEntity<List<LessonDto>> searchLesson(
            @RequestParam(value = "teacherId", required = false) String teacherId,
            @RequestParam(value = "schoolClassId", required = false) String schoolClassId) {
        Map<String, Object> criteria = new HashMap<>();
        criteria.put("teacherId", teacherId);
        criteria.put("schoolClassId", schoolClassId);
        return ResponseEntity.ok(lessonService.read(criteria));
    }
    //read


    // new version with hql
    @GetMapping(value = "/teacher")
    public ResponseEntity<List<SchoolClassDto>> searchLessonByTeacherId (
    		@RequestParam(value = "teacherId", required = false) String teacherId) {
    	 Map<String, Object> criteria = new HashMap<>();
    	if (teacherId != null) {
    		criteria.put("teacherId", teacherId);
    	}
    	return ResponseEntity.ok(lessonService.readByTeacher(criteria));
    }

    //update

    //delete
}
