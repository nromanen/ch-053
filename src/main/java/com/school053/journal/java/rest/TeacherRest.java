package com.school053.journal.java.rest;

import com.school053.journal.java.constant.Param;
import com.school053.journal.java.dto.TeacherDto;
import com.school053.journal.java.service.TeacherService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/teachers")
public class TeacherRest {
    // Following Create, Read, Update, and Delete (CRUD) operations:

    private final TeacherService teacherService;

    @Autowired
    public TeacherRest(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @ApiOperation(value = "Get teachers by criteria", notes = "we can get all teachers and teachers aren't curator",response = TeacherDto.class)
    @GetMapping(value = "/free-teacher")
    @ResponseBody
    public ResponseEntity<List<TeacherDto>> read(
            @RequestParam(value = Param.readTeacherNotCurator, required = false) boolean findFreeOnly) {
        Map<String, Object> criteria = new HashMap<>();
        criteria.put(Param.readTeacherNotCurator, findFreeOnly);
        return ResponseEntity.ok(teacherService.read(criteria));
    }
}
