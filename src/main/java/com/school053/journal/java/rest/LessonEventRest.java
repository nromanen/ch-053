package com.school053.journal.java.rest;

import com.school053.journal.java.constant.Param;
import com.school053.journal.java.dto.FileResourceDto;
import com.school053.journal.java.dto.FileResourceDto;
import com.school053.journal.java.dto.LessonEventDto;
import com.school053.journal.java.model.events.LessonEvent;
import com.school053.journal.java.model.upload.FileWithInfo;
import com.school053.journal.java.model.upload.FileWithInfoValidator;
import com.school053.journal.java.service.LessonEventService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.Valid;
import javax.ws.rs.PathParam;

@RestController
@RequestMapping(value = "/api/lesson-events")
public class LessonEventRest {
	private final LessonEventService lessonEventService;

	@Autowired
    FileWithInfoValidator fileValidator;

    @InitBinder("file")
    protected void initBinderFileBucket(WebDataBinder binder) {
        binder.setValidator(fileValidator);
    }

	@Autowired
	public LessonEventRest(LessonEventService lessonEventService) {
		this.lessonEventService = lessonEventService;
	}

    //create
    @ApiOperation(value = "Create lesson event", response = LessonEventDto.class)
    @PostMapping
    public ResponseEntity<LessonEventDto> create(@RequestBody LessonEventDto lessonEventDto) {
        return ResponseEntity.ok(lessonEventService.create(lessonEventDto));
    }

	@PostMapping(value = "/{eventId}/files")
	public ResponseEntity<?> createFile(@PathParam("eventId") String eventId, @Valid FileWithInfo file, BindingResult result) {
		FileResourceDto response = new FileResourceDto(file.getInfo(), "", "");
		try {
			byte[] bytes = file.getFile().getBytes();
			Path path = Paths.get(file.getFile().getOriginalFilename());
			Files.write(path, bytes);
		} catch (Exception ex) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
		}

		return ResponseEntity.ok(response);
	}

    @ApiOperation(value = "Get lesson events", response = LessonEventDto.class)
    @GetMapping(value = "/lessons")
    public ResponseEntity<List<LessonEventDto>> read(
            @RequestParam(value = Param.teacherId, required = false) String teacherId) {
        return ResponseEntity.ok(lessonEventService.read(teacherId));
    }

    @ApiOperation(value = "Get lesson event by id", response = LessonEventDto.class)
    @GetMapping(value = "/find")
    public ResponseEntity<LessonEventDto> find(@RequestParam String id) {
        return ResponseEntity.ok(lessonEventService.find(id));
    }

    //update
    @ApiOperation(value = "Update lesson event with merge in hibernate", response = ResponseEntity.class)
    @PostMapping(value = "/event")
    public ResponseEntity update(@RequestBody LessonEventDto lessonEventDto) {
        return ResponseEntity.ok(lessonEventService.update(lessonEventDto));
    }
}
