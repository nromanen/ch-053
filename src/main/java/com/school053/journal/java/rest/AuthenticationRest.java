package com.school053.journal.java.rest;

import com.school053.journal.java.exception.InvalidFormatException;
import com.school053.journal.java.jwt.JwtAuthenticationRequest;
import com.school053.journal.java.jwt.JwtUser;
import com.school053.journal.java.model.security.User;
import com.school053.journal.java.jwt.JwtAuthenticationResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.school053.journal.java.jwt.JwtTokenUtil;

import java.security.SecureRandom;

import javax.servlet.http.HttpServletRequest;


@RestController
public class AuthenticationRest {
	
	private CharSequence securityKey = "6C6F63616C2D6A6F75726E616C2D7365637265742D6B6579";
	private int strength = 10;
		
    @Value("Authorization")
    private String tokenHeader;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @ApiOperation(value = "Create token", response = ResponseEntity.class)
    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest) throws AuthenticationException {
        final Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authenticationRequest.getUsername(),
                            authenticationRequest.getPassword()
                    )
            );
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body("The username or password you’ve entered doesn’t match any account.");
        }
        User currentUser = new User();
        currentUser.setNickname(authenticationRequest.getUsername());
        currentUser.setPassword(authenticationRequest.getPassword());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken((JwtUser) userDetails);
        return ResponseEntity.ok(new JwtAuthenticationResponse(token));
    }

    @ApiOperation(value = "Checked token if we refresh page", response = ResponseEntity.class)
    @RequestMapping(value = "/refresh", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String refreshedToken = jwtTokenUtil.refreshToken(token);
        return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));

    }



}
