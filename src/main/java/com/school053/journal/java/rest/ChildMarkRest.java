package com.school053.journal.java.rest;

import com.school053.journal.java.constant.Param;
import com.school053.journal.java.dto.ChildMarkDto;
import com.school053.journal.java.dto.ChildMarkDto2;
import com.school053.journal.java.exception.InvalidFormatException;
import com.school053.journal.java.model.events.ChildMark;
import com.school053.journal.java.dto.LessonDto;
import com.school053.journal.java.dto.SubjectDto;
import com.school053.journal.java.service.ChildMarkService;

import io.swagger.annotations.ApiOperation;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/child-marks")
public class ChildMarkRest {
	private final ChildMarkService newChildMarkService;

	public ChildMarkRest(ChildMarkService newChildMarkService) {
		this.newChildMarkService = newChildMarkService;
	}

    //create
    @ApiOperation(value = "Create child mark", response = ResponseEntity.class)
    @PostMapping
    public ResponseEntity create(@RequestBody ChildMarkDto childMarkDto) throws InvalidFormatException {
        try {
            ChildMark childMark =  newChildMarkService.create(childMarkDto);
            return ResponseEntity.ok(childMark);
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //read
    @ApiOperation(value = "Get child marks by criteria", nickname = "ABC", response = ChildMarkDto.class)
    @GetMapping(value = "/date")
    public ResponseEntity<List<ChildMarkDto>> searchTasks(
            @RequestParam(value = "child-id", required = false) String childId,
            @RequestParam(value = "subject-id", required = false) String subjectId,
            @RequestParam(value = "start-date", required = false) @DateTimeFormat(pattern = "dd.MM.yyyy") LocalDate startDate,
            @RequestParam(value = "end-date", required = false) @DateTimeFormat(pattern = "dd.MM.yyyy") LocalDate endDate,
            @RequestParam(value = "lessonEventId", required = false) String lessonEventId
    ){
        Map<String, Object> criteria = new HashMap<>();
        if (childId != null) {
            criteria.put("childId", childId);
        }
        if (subjectId != null) {
            criteria.put("subjectId", subjectId);
        }
        if (startDate != null) {
            criteria.put("startDate", startDate);
        }
        if (endDate != null) {
            criteria.put("endDate", endDate);
        }
        if (lessonEventId != null) {
            criteria.put("lessonEventId", lessonEventId);
        }
        return ResponseEntity.ok(newChildMarkService.read(criteria));
    }

    @ApiOperation(value = "Get subjects by child id", response = ChildMarkDto.class)
    @GetMapping("/subjects")
	public ResponseEntity<List<SubjectDto>> searchSubjects(
			@RequestParam(value = "child-id", required = false) String childId) {
		Map<String, Object> criteria = new HashMap<>();
		if (childId != null) {
			criteria.put("childId", childId);
		}
		return ResponseEntity.ok(newChildMarkService.readSubject(criteria));
	}

    @ApiOperation(value = "Update child mark", response = ResponseEntity.class)
    //update
    @PostMapping(value = "/mark")
    public ResponseEntity update(@RequestBody ChildMarkDto childMarkDto) {
        try {
            return ResponseEntity.ok(newChildMarkService.update(childMarkDto));
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    
    //delete

}
