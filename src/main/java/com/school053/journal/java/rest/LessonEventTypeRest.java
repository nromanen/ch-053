package com.school053.journal.java.rest;

import com.school053.journal.java.model.events.LessonEventType;
import com.school053.journal.java.service.LessonEventTypeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/lesson-event-types")
public class LessonEventTypeRest {
    private final LessonEventTypeService lessonEventTypeService;

    @Autowired
    public LessonEventTypeRest(LessonEventTypeService lessonEventTypeService) {
        this.lessonEventTypeService = lessonEventTypeService;
    }

    @ApiOperation(value = "Get all lesson event types", response = LessonEventType.class)
    @GetMapping(value = "/type")
    public ResponseEntity<List<LessonEventType>> read() {
        return ResponseEntity.ok(lessonEventTypeService.read());
    }
}
