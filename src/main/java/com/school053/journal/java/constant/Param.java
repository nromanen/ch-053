package com.school053.journal.java.constant;

public class Param {
    public final static String searchSchoolClassByName = "byName";
    public final static String readTeacherNotCurator = "findFreeOnly";
    public final static String readChildByParentId = "parentId";
    public final static String teacherId = "teacherId";
    public final static String schoolClassId = "schoolClassId";
}
