package com.school053.journal.java.dto;

public class ChildMarkDto2 {
    private String id;
    private String absent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAbsent() {
        return absent;
    }

    public void setAbsent(String absent) {
        this.absent = absent;
    }
}
