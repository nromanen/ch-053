package com.school053.journal.java.dto;

import java.util.List;

public class ChildMarkDto {
	private String id;
	private String date;
	private String absent;
	private String mark;
	private String subject;
	private String lessonEventTypeName;
	private String comment;
	private String childId;
	private List<FileResourceDto> files;
	private String lessonEventTypeId;
	private String lessonEventId;

	private String firstName;
	private String lastName;

	public ChildMarkDto() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getLessonEventTypeName() {
		return lessonEventTypeName;
	}

	public void setLessonEventTypeName(String lessonEventTypeName) {
		this.lessonEventTypeName = lessonEventTypeName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<FileResourceDto> getFileResourcesDto() {
		return files;
	}

	public void setFileResourcesDto(List<FileResourceDto> fileResourcesDto) {
		this.files = fileResourcesDto;
	}

	public String getAbsent() {
		return absent;
	}

	public void setAbsent(String absent) {
		this.absent = absent;
	}

	public String getChildId() {
		return childId;
	}

	public void setChildId(String childId) {
		this.childId = childId;
	}

	public String getLessonEventTypeId() {
		return lessonEventTypeId;
	}

	public void setLessonEventTypeId(String lessonEventTypeId) {
		this.lessonEventTypeId = lessonEventTypeId;
	}

	public String getLessonEventId() {
		return lessonEventId;
	}

	public void setLessonEventId(String lessonEventId) {
		this.lessonEventId = lessonEventId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "ChildMarkDto [id=" + id + ", date=" + date + ", absent=" + absent + ", mark=" + mark + ", subject="
				+ subject + ", lessonEventTypeName=" + lessonEventTypeName + ", comment=" + comment + ", childId="
				+ childId + ", files=" + files + ", lessonEventTypeId=" + lessonEventTypeId + ", lessonEventId="
				+ lessonEventId + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}
}
