package com.school053.journal.java.dto;

public class LessonEventDto {

	private String id;
	private String comment;
	private String lessonId;
	private String eventDate;
	private boolean completed;
	private String completionDate;
	private String lessonEventTypeId;
	private String subjectName;
	private String schoolClassName;


	public LessonEventDto() {
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public String getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getLessonId() {
		return lessonId;
	}

	public void setLessonId(String lessonId) {
		this.lessonId = lessonId;
	}

	public String getLessonEventTypeId() {
		return lessonEventTypeId;
	}

	public void setLessonEventTypeId(String lessonEventTypeId) {
		this.lessonEventTypeId = lessonEventTypeId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getSchoolClassName() {
		return schoolClassName;
	}

	public void setSchoolClassName(String schoolClassName) {
		this.schoolClassName = schoolClassName;
	}
}
