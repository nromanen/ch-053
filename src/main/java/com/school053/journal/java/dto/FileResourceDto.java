package com.school053.journal.java.dto;

public class FileResourceDto {
	private String id;
	private String fileName;
	private String filePath;
	
	public FileResourceDto() {
	}

	public FileResourceDto(String id, String fileName, String filePath) {
		super();
		this.id = id;
		this.fileName = fileName;
		this.filePath = filePath;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}
