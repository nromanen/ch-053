package com.school053.journal.java.service;

import com.school053.journal.java.dto.SchoolClassDto;
import com.school053.journal.java.model.users.SchoolClass;

import java.util.List;
import java.util.Map;

public interface SchoolClassService {
    SchoolClass create(SchoolClassDto SchoolClassDto) throws Exception;
    List<SchoolClassDto> read(Map<String, Object> criteria);
}

