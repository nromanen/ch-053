package com.school053.journal.java.service.impl;

import com.school053.journal.java.dao.LessonDao;
import com.school053.journal.java.dto.LessonDto;
import com.school053.journal.java.dto.SchoolClassDto;
import com.school053.journal.java.mapper.LessonMapper;
import com.school053.journal.java.mapper.SchoolClassMapper;
import com.school053.journal.java.model.users.SchoolClass;
import com.school053.journal.java.service.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class LessonServiceImpl implements LessonService {
    private final LessonDao lessonDao;

    @Autowired
    public LessonServiceImpl(LessonDao lessonDao) {
        this.lessonDao = lessonDao;
    }

    @Override
    public List<LessonDto> read(Map<String, Object> criteria) {
        /*if (criteria.containsKey(Param.schoolClassId) && criteria.get(Param.schoolClassId) != null) {*/
        return lessonDao.search(criteria).stream().map(LessonMapper.MAPPER :: toDto).collect(Collectors.toList());
 //       }
   //     return null;
    }
    
    @Transactional
    @Override
    public List<SchoolClassDto> readByTeacher(Map<String, Object> criteria) {
    	return lessonDao.searchByTeacherId(criteria).stream().map(SchoolClassMapper.MAPPER :: toDto).collect(Collectors.toList());
    }
}
