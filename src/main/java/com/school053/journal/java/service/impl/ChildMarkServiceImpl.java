package com.school053.journal.java.service.impl;

import com.school053.journal.java.dao.ChildMarkDao;
import com.school053.journal.java.dto.ChildMarkDto;
import com.school053.journal.java.exception.InvalidFormatException;
import com.school053.journal.java.dto.LessonDto;
import com.school053.journal.java.dto.SubjectDto;
import com.school053.journal.java.mapper.ChildMarkMapper;
import com.school053.journal.java.model.events.ChildMark;
import com.school053.journal.java.mapper.LessonMapper;
import com.school053.journal.java.mapper.SubjectMapper;
import com.school053.journal.java.service.ChildMarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class ChildMarkServiceImpl implements ChildMarkService {
    private final String MARK = "^[1-9][0-9]?|0|100|null";
    private final Pattern PATTERN_MARK = Pattern.compile(MARK);

    private final ChildMarkDao childMarkDao;

    @Autowired
    public ChildMarkServiceImpl(ChildMarkDao childMarkDao) {
        this.childMarkDao = childMarkDao;
    }

    @Override
    @Transactional
    public ChildMark create(ChildMarkDto childMarkDto) throws InvalidFormatException {
        ChildMark childMark = ChildMarkMapper.MAPPER.fromDto(childMarkDto);
        Matcher matcher = PATTERN_MARK.matcher(childMarkDto.getMark());
        if (!matcher.matches()) {
            throw new InvalidFormatException("Mark must be from 0 to 100");
        }
        childMarkDao.create(childMark);
        return childMark;
    }

    @Transactional
    @Override
    public List<ChildMarkDto> read(Map<String, Object> criteria) {
            return childMarkDao.searchMarks(criteria).stream()
                    .map(ChildMarkMapper.MAPPER::toDto).sorted(Comparator.comparing(ChildMarkDto::getLastName).thenComparing(ChildMarkDto::getFirstName)).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public ChildMark update(ChildMarkDto childMarkDto) throws InvalidFormatException {
        ChildMark childMark = ChildMarkMapper.MAPPER.fromDto(childMarkDto);
        if (childMarkDto.getMark() == null) {
            childMark.setMark(null);
        }
        if (childMarkDto.getMark() != null) {
            Matcher matcher = PATTERN_MARK.matcher(childMarkDto.getMark());
            if (!matcher.matches()) {
                throw new InvalidFormatException("Mark must be from 0 to 100");
            }
        }
        childMarkDao.update(childMark);
        return childMark;
    }

    @Transactional
    @Override
    public List <SubjectDto> readSubject (Map<String, Object> criteria) {
    	return childMarkDao.searchSubject(criteria).stream()
    			.map(SubjectMapper.MAPPER::toDto).collect(Collectors.toList());
    }



}
