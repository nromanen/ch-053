package com.school053.journal.java.service.impl;

import com.school053.journal.java.constant.Param;
import com.school053.journal.java.dao.ChildDao;
import com.school053.journal.java.dto.ChildDto;
import com.school053.journal.java.mapper.ChildMapper;
import com.school053.journal.java.service.ChildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ChildServiceImpl implements ChildService {
    private final ChildDao childDao;

    @Autowired
    public ChildServiceImpl(ChildDao childDao) {
        this.childDao = childDao;
    }

    @Override
    public List<ChildDto> read(Map<String, Object> criteria) {
        if (criteria.containsKey(Param.readChildByParentId) && criteria.get(Param.readChildByParentId) != null) {
            return childDao.fetchByParent((String) criteria.get(Param.readChildByParentId)).stream().map(ChildMapper.MAPPER :: toDto).collect(Collectors.toList());
        }
        if (criteria.containsKey(Param.schoolClassId) && criteria.get(Param.schoolClassId) != null) {
            return childDao.search(criteria).stream().map(ChildMapper.MAPPER :: toDto).collect(Collectors.toList());
        }
        return null;
    }
}
