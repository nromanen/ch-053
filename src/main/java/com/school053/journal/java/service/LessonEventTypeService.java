package com.school053.journal.java.service;

import com.school053.journal.java.model.events.LessonEventType;

import java.util.List;

public interface LessonEventTypeService {
    List<LessonEventType> read();
}
