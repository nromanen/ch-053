package com.school053.journal.java.service;

import com.school053.journal.java.dto.TeacherDto;

import java.util.List;
import java.util.Map;

public interface TeacherService {
    List<TeacherDto> read(Map<String, Object> criteria);
}
