package com.school053.journal.java.service;

import com.school053.journal.java.dto.LessonDto;
import com.school053.journal.java.dto.SchoolClassDto;
import com.school053.journal.java.model.users.SchoolClass;

import java.util.List;
import java.util.Map;

public interface LessonService {
    List<LessonDto> read(Map<String, Object> criteria);

	List<SchoolClassDto> readByTeacher(Map<String, Object> criteria);
}
