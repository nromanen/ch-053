package com.school053.journal.java.service.impl;

import com.school053.journal.java.constant.Param;
import com.school053.journal.java.dao.ChildDao;
import com.school053.journal.java.dao.ChildMarkDao;
import com.school053.journal.java.dao.LessonDao;
import com.school053.journal.java.dao.LessonEventDao;
import com.school053.journal.java.dto.LessonEventDto;
import com.school053.journal.java.mapper.LessonEventMapper;
import com.school053.journal.java.model.events.ChildMark;
import com.school053.journal.java.model.events.Lesson;
import com.school053.journal.java.model.events.LessonEvent;
import com.school053.journal.java.model.events.LessonEventType;
import com.school053.journal.java.model.users.Child;
import com.school053.journal.java.service.LessonEventService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LessonEventServiceImpl implements LessonEventService {
    private final LessonEventDao lessonEventDao;
    private final ChildMarkDao childMarkDao;
    private final ChildDao childDao;
    private final LessonDao lessonDao;

    @Autowired
    public LessonEventServiceImpl(LessonEventDao lessonEventDao, ChildMarkDao childMarkDao, ChildDao childDao, LessonDao lessonDao) {
        this.lessonEventDao = lessonEventDao;
        this.childMarkDao = childMarkDao;
        this.childDao = childDao;
        this.lessonDao = lessonDao;
    }

    @Transactional
    @Override
    public LessonEventDto create(LessonEventDto lessonEventDto) {
        LessonEvent lessonEvent = LessonEventMapper.MAPPER.fromDto(lessonEventDto);
        if (lessonEvent.getComment() == null) { lessonEvent.setComment("");}
        String lessonEventId = lessonEventDao.createLessonEvent(lessonEvent);
        lessonEvent.setId(lessonEventId);
        Lesson lesson =  lessonDao.fetch(lessonEvent.getLesson().getId());
        LessonEventType lessonEventType = new LessonEventType();
        lessonEventType.setId(lessonEventDto.getLessonEventTypeId());
        Map<String, Object> criteria = new HashMap<>();
        criteria.put(Param.schoolClassId, lesson.getSchoolClass().getId());
        List<Child> children = childDao.search(criteria);
        List<ChildMark> childMarks = new ArrayList<>();
        for(Child child : children) {
            ChildMark childMark = new ChildMark();
            childMark.setChild(child);
            childMark.setAbsent(false);
            childMark.setLessonEvent(lessonEvent);
            childMark.setLessonEventType(lessonEventType);
            childMarkDao.create(childMark);
            childMarks.add(childMark);
        }
        return LessonEventMapper.MAPPER.toDto(lessonEvent);
    }

    @Transactional
    @Override
    public List<LessonEventDto> read(String teacherId) {
        return lessonEventDao.fetchByTeacher(teacherId).stream().map(LessonEventMapper.MAPPER :: toDto).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public LessonEventDto update(LessonEventDto lessonEventDto) {
        LessonEvent lessonEvent = LessonEventMapper.MAPPER.fromDto(lessonEventDto);

        if (lessonEvent.getComment() == null) { lessonEvent.setComment("");}

        lessonEvent.setCompleted(true);
        lessonEventDao.update(lessonEvent);
        return lessonEventDto;
    }

    @Override
    @Transactional
    public LessonEventDto find(String id) {
        return LessonEventMapper.MAPPER.toDto(lessonEventDao.fetch(id));
    }
}
