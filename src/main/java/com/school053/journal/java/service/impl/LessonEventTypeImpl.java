package com.school053.journal.java.service.impl;

import com.school053.journal.java.dao.LessonEventTypeDao;
import com.school053.journal.java.model.events.LessonEventType;
import com.school053.journal.java.service.LessonEventTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LessonEventTypeImpl implements LessonEventTypeService {
    private final LessonEventTypeDao lessonEventTypeDao;

    @Autowired
    public LessonEventTypeImpl(LessonEventTypeDao lessonEventTypeDao) {
        this.lessonEventTypeDao = lessonEventTypeDao;
    }

    @Override
    public List<LessonEventType> read() {
        return lessonEventTypeDao.fetchAll();
    }
}
