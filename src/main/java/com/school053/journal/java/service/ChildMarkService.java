package com.school053.journal.java.service;

import com.school053.journal.java.dto.ChildMarkDto;
import com.school053.journal.java.dto.ChildMarkDto2;
import com.school053.journal.java.exception.InvalidFormatException;
import com.school053.journal.java.model.events.ChildMark;
import com.school053.journal.java.dto.LessonDto;
import com.school053.journal.java.dto.SubjectDto;

import java.util.List;
import java.util.Map;

public interface ChildMarkService {
    ChildMark create(ChildMarkDto childMarkDto) throws InvalidFormatException;
    List<ChildMarkDto> read(Map<String, Object> criteria);
    ChildMark update(ChildMarkDto childMarkDto) throws InvalidFormatException;
    List<SubjectDto> readSubject(Map<String, Object> criteria);
}
