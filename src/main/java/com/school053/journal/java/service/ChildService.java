package com.school053.journal.java.service;

import com.school053.journal.java.dto.ChildDto;

import java.util.List;
import java.util.Map;

public interface ChildService {
    List<ChildDto> read(Map<String, Object> criteria);
}
