package com.school053.journal.java.service.impl;

        import com.school053.journal.java.constant.Param;
        import com.school053.journal.java.dao.SchoolClassDao;
        import com.school053.journal.java.dto.SchoolClassDto;
        import com.school053.journal.java.exception.InvalidFormatException;
        import com.school053.journal.java.mapper.SchoolClassMapper;
        import com.school053.journal.java.model.users.SchoolClass;
        import com.school053.journal.java.service.SchoolClassService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;
        import org.springframework.transaction.annotation.Transactional;

        import java.util.Comparator;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;
        import java.util.regex.Matcher;
        import java.util.regex.Pattern;
        import java.util.stream.Collectors;

@Service
public class SchoolClassServiceImpl implements SchoolClassService {
        private final String NAME_SCHOOL_CLASS = "^[1-9][a-z]?|[1][0-2][a-z]?";
        private final String NAME = "^[1-9][a-z]+";
        private final String EMAIL = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
        private final String TELEPHONE = "[+380][0-9]{9}";
        private final Pattern PATTERN_NAME_SCHOOL_CLASS = Pattern.compile(NAME_SCHOOL_CLASS);
        private final Pattern PATTERN_NAME = Pattern.compile(NAME);
        private final Pattern PATTERN_EMAIL = Pattern.compile(EMAIL);
        private final Pattern PATTERN_TELEPHONE = Pattern.compile(TELEPHONE);

        private final SchoolClassDao schoolClassDao;

        @Autowired
        public SchoolClassServiceImpl(SchoolClassDao schoolClassDao) {
                this.schoolClassDao = schoolClassDao;
        }

        @Transactional
        @Override
        public SchoolClass create(SchoolClassDto schoolClassDto) throws Exception {
                SchoolClass schoolClass = SchoolClassMapper.MAPPER.fromDto(schoolClassDto);
                Matcher matcher = PATTERN_NAME_SCHOOL_CLASS.matcher(schoolClass.getName());
                Map<String, Object> criteria = new HashMap<>();
                criteria.put(Param.searchSchoolClassByName, schoolClass.getName());
                if (!matcher.matches()) {
                        throw new InvalidFormatException("Name should be in format 1-12 + a-z");
                }
                if (!schoolClassDao.searchSchoolClass(criteria).isEmpty()) {
                        throw new InvalidFormatException("The name already exist");
                }
                schoolClassDao.create(schoolClass);
                return schoolClass;
        }

        @Override
        public List<SchoolClassDto> read(Map<String, Object> criteria) {
                if (criteria.containsKey(Param.teacherId) && criteria.get(Param.teacherId) != null) {
                        return schoolClassDao.search(criteria).stream().map(SchoolClassMapper.MAPPER :: toDto).collect(Collectors.toList());
                }
                return schoolClassDao.fetchAll().stream().map(SchoolClassMapper.MAPPER :: toDto).sorted(Comparator.comparing(SchoolClassDto::getName)).collect(Collectors.toList());
        }
}
