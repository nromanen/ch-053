package com.school053.journal.java.service.impl;

import com.school053.journal.java.constant.Param;
import com.school053.journal.java.dao.TeacherDao;
import com.school053.journal.java.dto.TeacherDto;
import com.school053.journal.java.mapper.TeacherMapper;
import com.school053.journal.java.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class TeacherServiceImpl implements TeacherService {

    private final TeacherDao teacherDao;

    @Autowired
    public TeacherServiceImpl(TeacherDao teacherDao) {
        this.teacherDao = teacherDao;
    }

    @Override
    public List<TeacherDto> read(Map<String, Object> criteria) {
        if (criteria.containsKey(Param.readTeacherNotCurator) && criteria.get(Param.readTeacherNotCurator) != null) {
            return teacherDao.searchTeacher(criteria).stream().map(TeacherMapper.MAPPER::toDto).collect(Collectors.toList());
        }
        return teacherDao.fetchAll().stream().map(TeacherMapper.MAPPER::toDto).collect(Collectors.toList());

    }
}
