package com.school053.journal.java.service;

import com.school053.journal.java.dto.LessonEventDto;
import com.school053.journal.java.model.events.LessonEvent;

import java.util.List;

public interface LessonEventService {
    LessonEventDto create(LessonEventDto lessonEventDto);
    List<LessonEventDto> read(String teacherId);
    LessonEventDto update(LessonEventDto lessonEventDto);
    LessonEventDto find(String id);
}