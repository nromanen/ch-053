package com.school053.journal.java.dao.impl;

import com.school053.journal.java.dao.AbstractDao;
import com.school053.journal.java.dao.ChildDao;
import com.school053.journal.java.model.users.Child;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository(value = "ChildDao")
public class ChildDaoImpl extends AbstractDao<Child> implements ChildDao {

    @Override
    public List<Child> fetchActive() {
        return entityManager
                .createNamedQuery(Child.FIND_ACTIVE, Child.class)
                .getResultList();
    }

    @Override
    public List<Child> fetchByClass(String classId) {
        return entityManager
                .createNamedQuery(Child.FIND_BY_CLASS, Child.class)
                .setParameter("classId", classId)
                .getResultList();
    }

    @Override
    public List<Child> fetchByParent(String parentId) {
        return entityManager
                .createNamedQuery(Child.FIND_BY_PARENT, Child.class)
                .setParameter("parentId", parentId)
                .getResultList();
    }

    @Override
    public List<Child> search(Map<String, Object> criteria) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Child> childCriteriaQuery = criteriaBuilder.createQuery(Child.class);
        Root<Child> childRoot = childCriteriaQuery.from(Child.class);
        childCriteriaQuery.select(childRoot);
        List<Predicate> predicateList = new ArrayList<>();
        if (criteria.containsKey("schoolClassId") && criteria.get("schoolClassId") != null) {
            predicateList.add(criteriaBuilder.equal(childRoot.get("schoolClass").get("id"), criteria.get("schoolClassId")));
        }
        if (!predicateList.isEmpty()) {
            childCriteriaQuery.where(predicateList.toArray(new Predicate[predicateList.size()]));
        }
        return entityManager.createQuery(childCriteriaQuery).getResultList();
    }


}
