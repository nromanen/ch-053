package com.school053.journal.java.dao;

import com.school053.journal.java.model.events.LessonEventType;

public interface LessonEventTypeDao extends InterfaceDao<LessonEventType> {
}
