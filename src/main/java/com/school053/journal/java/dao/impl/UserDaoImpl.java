package com.school053.journal.java.dao.impl;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.school053.journal.java.dao.AbstractDao;
import com.school053.journal.java.dao.UserDao;
import com.school053.journal.java.model.security.User;



@Repository(value = "UserDao")
public class UserDaoImpl extends AbstractDao<User> implements UserDao {

    public User findUserByUsername(String nickname) {
        TypedQuery<User> query = entityManager
                .createNamedQuery(User.FIND_BY_USERNAME, User.class).setParameter("nickname", nickname);
        return query.getSingleResult();
    }

    @Override
    public User findUserByEmail(String email) {
        return entityManager
                .createNamedQuery(User.FIND_BY_EMAIL, User.class)
                .setParameter("email", email).getSingleResult();
    }

    @Override
    public boolean ifNicknameExist(String nickname, String id) {
        return  entityManager
                .createNamedQuery(User.FIND_EXISTING_NICKNAME, User.class)
                .setParameter("nickname", nickname)
                .setParameter("id", id)
                .getResultList()
                .isEmpty();
    }

    @Override
    public boolean ifEmailExist(String email, String id) {
        return entityManager
                .createNamedQuery(User.FIND_EXISTING_EMAIL, User.class)
                .setParameter("email", email)
                .setParameter("id", id)
                .getResultList()
                .isEmpty();
    }

    @Override
    public Long countByNickname(String nickname) {
        return entityManager
                .createNamedQuery(User.COUNT_BY_USERNAME, Long.class)
                .setParameter("nickname", nickname)
                .getSingleResult();
    }

    @Override
    public Long countByEmail(String email) {
        return entityManager
                .createNamedQuery(User.COUNT_BY_EMAIL, Long.class)
                .setParameter("email", email)
                .getSingleResult();
    }
}
