package com.school053.journal.java.dao.impl;

import org.springframework.stereotype.Repository;

import com.school053.journal.java.dao.AbstractDao;
import com.school053.journal.java.dao.AuthorityDao;
import com.school053.journal.java.model.security.Authority;



@Repository
public class AuthorityDaoImpl extends AbstractDao<Authority> implements AuthorityDao{

    @Override
    public Authority findOne(Authority.AuthorityType type) {
        return entityManager.find(Authority.class , type );
    }

}

