package com.school053.journal.java.dao.impl;

import com.school053.journal.java.constant.Param;
import com.school053.journal.java.dao.AbstractDao;
import com.school053.journal.java.dao.SchoolClassDao;
import com.school053.journal.java.model.users.SchoolClass;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Repository(value = "SchoolClassDao")
public class SchoolClassDaoImpl extends AbstractDao<SchoolClass> implements SchoolClassDao {

    @Override
    public List<SchoolClass> fetchActiveByName() {
        return entityManager
                .createNamedQuery(SchoolClass.FIND_ACTIVE_BY_NAME, SchoolClass.class)
                .getResultList();
    }

    @Override
    public List<SchoolClass> searchSchoolClass(Map<String, Object> criteria) {
        StringBuilder query = new StringBuilder("FROM SchoolClass sc");
        List<SchoolClass> schoolClasses = new ArrayList<>();
        if (criteria.containsKey("start") && criteria.containsKey("countAll") && criteria.get("start") != null && criteria.get("countAll") != null) {
            schoolClasses = entityManager.createQuery(query.toString(), SchoolClass.class).setFirstResult((Integer) criteria.get("start")).setMaxResults((Integer) criteria.get("countAll")).getResultList();
        }
        else if (criteria.containsKey("byName")) {
            query.append(" WHERE sc.name = '").append(criteria.get(Param.searchSchoolClassByName).toString());
            query.append("'");
            schoolClasses = entityManager.createQuery(query.toString(), SchoolClass.class).getResultList();
        }
        else {
            schoolClasses = entityManager.createQuery(query.toString(), SchoolClass.class).getResultList();
        }
        return schoolClasses;
    }

    @Override
    public List<SchoolClass> search(Map<String, Object> criteria) {
        StringBuilder query = new StringBuilder("SELECT sc FROM SchoolClass sc");
        List<SchoolClass> schoolClasses = new ArrayList<>();
        if (criteria.containsKey(Param.teacherId) && criteria.get(Param.teacherId) != null) {
            query.append(" JOIN Lesson l ON sc.id = l.schoolClass.id WHERE l.teacher.id = '").append(criteria.get(Param.teacherId).toString());
            query.append("'");
        }
        schoolClasses = entityManager.createQuery(query.toString(), SchoolClass.class).getResultList();
        Set<SchoolClass> schoolClassesSet = new HashSet<>(schoolClasses);
        schoolClasses = new ArrayList<>(schoolClassesSet);
        return schoolClasses;
    }
}
