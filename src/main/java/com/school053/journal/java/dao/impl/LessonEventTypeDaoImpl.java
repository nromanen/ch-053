package com.school053.journal.java.dao.impl;

import com.school053.journal.java.dao.AbstractDao;
import com.school053.journal.java.dao.LessonEventTypeDao;
import com.school053.journal.java.model.events.LessonEvent;
import com.school053.journal.java.model.events.LessonEventType;
import org.springframework.stereotype.Repository;


@Repository(value = "LessonEventTypeDao")
public class LessonEventTypeDaoImpl extends AbstractDao<LessonEventType> implements LessonEventTypeDao{
}
