package com.school053.journal.java.dao.impl;

import com.school053.journal.java.dao.AbstractDao;
import com.school053.journal.java.dao.LessonDao;
import com.school053.journal.java.model.events.Lesson;
import com.school053.journal.java.model.users.SchoolClass;

import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository(value = "LessonDao")
public class LessonDaoImpl extends AbstractDao<Lesson> implements LessonDao {

    @Override
    public List<Lesson> search(Map<String, Object> criteria) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Lesson> criteriaQuery = criteriaBuilder.createQuery(Lesson.class);
        Root<Lesson> lessonRoot = criteriaQuery.from(Lesson.class);
        criteriaQuery.select(lessonRoot);
        List<Predicate> predicateList = new ArrayList<>();
        System.out.println(criteria.values());
        if (criteria.containsKey("teacherId") && criteria.get("teacherId") != null) {
            predicateList.add(criteriaBuilder.equal(lessonRoot.get("teacher").get("id"), criteria.get("teacherId")));
        }
        if (criteria.containsKey("schoolClassId") && criteria.get("schoolClassId") != null) {
            predicateList.add(criteriaBuilder.equal(lessonRoot.get("schoolClass").get("id"), criteria.get("schoolClassId")));
        }
        if (!predicateList.isEmpty()) {
            criteriaQuery.where(predicateList.toArray(new Predicate[predicateList.size()]));
        }
        return entityManager.createQuery(criteriaQuery).getResultList();
    }
    
    @Override
    public List<SchoolClass> searchByTeacherId (Map<String, Object> criteria) {
    	StringBuilder query = new StringBuilder ("SELECT DISTINCT l.schoolClass FROM Lesson l LEFT JOIN FETCH l.schoolClass.lessons");
    	Query q = null;
    	if (criteria.containsKey("teacherId")) {
    		query.append(" WHERE l.teacher.id = :teacherId");
			q = entityManager.createQuery(query.toString()).setParameter("teacherId", criteria.get("teacherId"));
		} else {
			q = entityManager.createQuery(query.toString());
    	
		}
    	return q.getResultList();
    }
}
