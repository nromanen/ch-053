package com.school053.journal.java.dao.impl;

import com.school053.journal.java.dao.AbstractDao;
import com.school053.journal.java.dao.TeacherDao;
import com.school053.journal.java.model.users.Teacher;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository(value = "TeacherDao")
public class TeacherDaoImpl extends AbstractDao<Teacher> implements TeacherDao {
    @Override
    public List<Teacher> fetchTeacherNotCurator() {
        return entityManager.createNamedQuery(Teacher.FIND_NOT_CURATOR, Teacher.class).getResultList();
    }

    @Override
    public List<Teacher> searchTeacher(Map<String, Object> criteria) {
        StringBuilder query = new StringBuilder("FROM Teacher t");
        if (criteria.containsKey("findFreeOnly") && criteria.get("findFreeOnly").equals(true)) {
            query.append(" where t.id not in (select curator.id from SchoolClass)");
        }
        return entityManager.createQuery(query.toString(), Teacher.class).getResultList();
}
}
