package com.school053.journal.java.dao;

import com.school053.journal.java.model.users.SchoolClass;

import java.util.List;
import java.util.Map;

public interface SchoolClassDao extends InterfaceDao<SchoolClass> {
    List<SchoolClass> fetchActiveByName();
    List<SchoolClass> searchSchoolClass(Map<String, Object> criteria);
    List<SchoolClass> search(Map<String, Object> criteria);
}
