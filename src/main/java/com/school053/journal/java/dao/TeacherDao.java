package com.school053.journal.java.dao;

import com.school053.journal.java.model.users.Teacher;

import java.util.List;
import java.util.Map;

public interface TeacherDao extends InterfaceDao<Teacher> {
    List<Teacher> fetchTeacherNotCurator();
    List<Teacher> searchTeacher(Map<String, Object> criteria);
}
