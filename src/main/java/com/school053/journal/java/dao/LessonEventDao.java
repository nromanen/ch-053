package com.school053.journal.java.dao;

import java.util.List;

import com.school053.journal.java.model.events.LessonEvent;

public interface LessonEventDao extends InterfaceDao<LessonEvent>{
	
	List<LessonEvent> fetchBySubjectId(String subjectId);
	List<LessonEvent> fetchByTeacher(String teacherId);
	String createLessonEvent(LessonEvent lessonEvent);
	
}
