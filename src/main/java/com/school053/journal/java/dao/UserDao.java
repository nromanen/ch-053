package com.school053.journal.java.dao;

import com.school053.journal.java.dao.InterfaceDao;
import com.school053.journal.java.model.security.User;

public interface UserDao extends InterfaceDao<User> {

    User findUserByUsername(String nickname);
    User findUserByEmail(String email);

    boolean ifNicknameExist(String nickname, String id);

    boolean ifEmailExist(String email, String id);

    Long countByNickname(String nickname);

    Long countByEmail(String email);

}
