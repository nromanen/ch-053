package com.school053.journal.java.dao.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.school053.journal.java.dao.AbstractDao;
import com.school053.journal.java.dao.ChildMarkDao;
import com.school053.journal.java.model.events.ChildMark;
import com.school053.journal.java.model.events.Subject;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Repository(value = "ChildMarkDao")
public class ChildMarkDaoImpl extends AbstractDao<ChildMark> implements ChildMarkDao {

	public List<ChildMark> searchMarks(Map<String, Object> searchCriterias) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ChildMark> criteriaQuery = builder.createQuery(ChildMark.class);
		Root<ChildMark> childMarkRoot = criteriaQuery.from(ChildMark.class);
		criteriaQuery.select(childMarkRoot);
		List<Predicate> searchPredicates = new ArrayList<>();

		if(searchCriterias.containsKey("childId") && searchCriterias.get("childId") != null){
			searchPredicates.add(builder.equal(childMarkRoot.get("child").get("id"),searchCriterias.get("childId")));
		}

		if(searchCriterias.containsKey("subjectId") && searchCriterias.get("subjectId") != null){
			searchPredicates.add(builder.equal(
					childMarkRoot.get("lessonEvent").get("lesson").get("subject").get("id"),
					searchCriterias.get("subjectId")
			));
		}

		if(
				(searchCriterias.containsKey("startDate") && searchCriterias.get("startDate") != null) &&
				(searchCriterias.containsKey("endDate") && searchCriterias.get("endDate") != null)
		){
			LocalDate startDate = (LocalDate) searchCriterias.get("startDate");
			LocalDate endDate = (LocalDate) searchCriterias.get("endDate");
			searchPredicates.add(builder.between(childMarkRoot.get("lessonEvent").get("eventDate"), startDate, endDate));
		}
		if (
				(searchCriterias.containsKey("lessonEventId") && searchCriterias.get("lessonEventId") != null)
				) {
					searchPredicates.add(builder.equal(childMarkRoot.get("lessonEvent").get("id"), searchCriterias.get("lessonEventId")));
		}

		if(!searchPredicates.isEmpty()){
			criteriaQuery.where(searchPredicates.toArray(new Predicate[searchPredicates.size()]));
		}

		criteriaQuery.orderBy(builder.desc(childMarkRoot.get("lessonEvent").get("eventDate"))); 
		return entityManager.createQuery(criteriaQuery).getResultList();
	}

	@Override
	public List<Subject> searchSubject(Map<String, Object> criteria) {
		
		StringBuilder query = new StringBuilder("SELECT DISTINCT cm.lessonEvent.lesson.subject FROM ChildMark cm");
		Query q = null;
		if (criteria.containsKey("childId")) {
			query.append(" WHERE cm.child.id = :childId");
			q = entityManager.createQuery(query.toString()).setParameter("childId", criteria.get("childId"));
		} else {
			q = entityManager.createQuery(query.toString());
		}
		return q.getResultList();
	}


}
