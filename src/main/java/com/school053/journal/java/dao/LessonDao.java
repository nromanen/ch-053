package com.school053.journal.java.dao;

import com.school053.journal.java.model.events.Lesson;
import com.school053.journal.java.model.users.SchoolClass;

import java.util.List;
import java.util.Map;

public interface LessonDao extends InterfaceDao<Lesson> {
    List<Lesson> search(Map<String, Object> criteria);

	List<SchoolClass> searchByTeacherId(Map<String, Object> criteria);
}
