package com.school053.journal.java.dao;

import com.school053.journal.java.model.security.Authority;

public interface AuthorityDao extends InterfaceDao<Authority>{

    Authority findOne(Authority.AuthorityType type);

}
