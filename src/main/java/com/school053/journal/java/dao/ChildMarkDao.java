package com.school053.journal.java.dao;

import java.util.List;
import java.util.Map;

import com.school053.journal.java.model.events.ChildMark;
import com.school053.journal.java.model.events.Subject;

public interface ChildMarkDao extends InterfaceDao<ChildMark> {
	
    List<ChildMark> searchMarks(Map<String, Object> criterias);
    List<Subject> searchSubject(Map<String, Object> criteria);
    
}
