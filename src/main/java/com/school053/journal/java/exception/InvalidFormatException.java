package com.school053.journal.java.exception;

public class InvalidFormatException extends Exception {
    public InvalidFormatException() {}
    public InvalidFormatException(String m) {
        super(m);
    }
}
