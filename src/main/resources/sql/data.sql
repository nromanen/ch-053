
INSERT INTO users (id, nickname, password, active, first_name, last_name, patronymic, phone_number, email) VALUES('200', 'teacher1', '$2a$10$LNN9IAXzujgcy.NtpkFYie3Ay4u3Xtm9lnTn6Q8f7uc1iPYlh.T8G', TRUE, 'Megan', 'Jordan', 'Cornett','+380994789623', 'email@gmail.com');

INSERT INTO users (id, nickname, password, active, first_name, last_name, patronymic, phone_number, email) ('201', 'admin', '$2a$10$3RaZqHqE.U1Fd0v79xe6aOrg5vEtvvdxJKuuPYgh9OydmSEBNTM/u', TRUE, 'Paul', 'Ward', 'Campbell',    '+380745691281', 'campward@gmail.com');

INSERT INTO users (id, nickname, password, active, first_name, last_name, patronymic, phone_number, email) ('202', 'parent1', '$2a$10$8fpc82lh/Gay2R47bkGor.YzNSO5qUSpFOANRW5L6lpg.wlmCiJia', TRUE, 'Steve', 'Parker', 'Matthews',    '+380696324281', 'mattp@gmail.com');

INSERT INTO users (id, nickname, password, active, first_name, last_name, patronymic, phone_number, email) ('203', 'parent2', '$2a$10$LNN9IAXzujgcy.NtpkFYie3Ay4u3Xtm9lnTn6Q8f7uc1iPYlh.T8G', TRUE, 'Albert', 'Smith', 'Dixon',    '+380659459881', 'name@gmail.com');

INSERT INTO users (id, nickname, password, active, first_name, last_name, patronymic, phone_number, email) ('204', 'child1', '$2a$10$LNN9IAXzujgcy.NtpkFYie3Ay4u3Xtm9lnTn6Q8f7uc1iPYlh.T8G', TRUE, 'John', 'Williams', 'Russell',    '+380658954281', 'wilrust@gmail.com');

INSERT INTO users (id, nickname, password, active, first_name, last_name, patronymic, phone_number, email) ('205', 'child2', '$2a$10$LNN9IAXzujgcy.NtpkFYie3Ay4u3Xtm9lnTn6Q8f7uc1iPYlh.T8G', TRUE, 'David', 'Evans', 'Fisher',    '+380669874281', 'evfish@gmail.com');

INSERT INTO users (id, nickname, password, active, first_name, last_name, patronymic, phone_number, email) ('206', 'child3', '$2a$10$LNN9IAXzujgcy.NtpkFYie3Ay4u3Xtm9lnTn6Q8f7uc1iPYlh.T8G', TRUE, 'Matt', 'Walker', 'Pearson',    '+380612345681', 'walkpear@gmail.com');

INSERT INTO users (id, nickname, password, active, first_name, last_name, patronymic, phone_number, email) ('207', 'child4', '$2a$10$LNN9IAXzujgcy.NtpkFYie3Ay4u3Xtm9lnTn6Q8f7uc1iPYlh.T8G', TRUE, 'Jack', 'Lewis', 'Palmer',    '+380684365281', 'palmlew@gmail.com');

INSERT INTO users (id, nickname, password, active, first_name, last_name, patronymic, phone_number, email) ('208', 'unknown', '$2a$10$LNN9IAXzujgcy.NtpkFYie3Ay4u3Xtm9lnTn6Q8f7uc1iPYlh.T8G', TRUE, 'Mike', 'Clarke', 'Walsh',    '+380823954281', 'clarkew@gmail.com');

INSERT INTO sec_user_authority (user_id, authority_name) VALUES '200', 'TEACHER');

INSERT INTO sec_user_authority (user_id, authority_name) VALUES '201', 'ADMIN');

INSERT INTO sec_user_authority (user_id, authority_name) VALUES '201', 'TEACHER');

INSERT INTO sec_user_authority (user_id, authority_name) VALUES '202', 'PARENT');

INSERT INTO sec_user_authority (user_id, authority_name) VALUES '203', 'PARENT');

INSERT INTO sec_user_authority (user_id, authority_name) VALUES '204', 'CHILD');

INSERT INTO sec_user_authority (user_id, authority_name) VALUES '205', 'CHILD');

INSERT INTO sec_user_authority (user_id, authority_name) VALUES '206', 'CHILD');

INSERT INTO sec_user_authority (user_id, authority_name) VALUES '207', 'CHILD');

INSERT INTO teachers (id, description) VALUES ('200', 'Just teacher');

INSERT INTO teachers (id, description) VALUES ('201', 'Head of studies');

INSERT INTO school_classes (id, name, studying_end, studying_start, active, curator_id) VALUES('1', '6a', '01-01-2017', '01-01-2020', TRUE, '200');

INSERT INTO school_classes (id, name, studying_end, studying_start, active, curator_id) VALUES ('2', '7a', '01-01-2017', '01-01-2020', TRUE, '201');

INSERT INTO childs (id, school_class_id) VALUES('207', '1');

INSERT INTO childs (id, school_class_id) VALUES ('204', '1');

INSERT INTO childs (id, school_class_id) VALUES ('205', '2');

INSERT INTO childs (id, school_class_id) VALUES ('206', '2');

INSERT INTO parents (id, job) VALUES('202', 'job');

INSERT INTO parents (id, job) VALUES('203', 'job');

INSERT INTO relationship (parent_id, child_id) VALUES('202', '204');

INSERT INTO relationship (parent_id, child_id) VALUES('202', '205');

INSERT INTO relationship (parent_id, child_id) VALUES ('203', '207');

INSERT INTO relationship (parent_id, child_id) VALUES ('203', '206');

INSERT INTO subjects (id, name, alias) VALUES('1', 'math', 'math');

INSERT INTO subjects (id, name, alias) VALUES('2', 'biology', 'biology');

INSERT INTO subjects (id, name, alias) VALUES('3', 'art', 'art');

INSERT INTO subjects (id, name, alias) VALUES('4', 'english', 'english');

INSERT INTO lessons (id, teacher_id, subject_id, class_id) VALUES ('1', '200', '1', '1');

INSERT INTO lessons (id, teacher_id, subject_id, class_id) VALUES ('2', '201', '2', '1');

INSERT INTO lessons (id, teacher_id, subject_id, class_id) VALUES ('3', '200', '4', '1');

INSERT INTO lessons (id, teacher_id, subject_id, class_id) VALUES ('4', '200', '3', '2');

INSERT INTO lessons (id, teacher_id, subject_id, class_id) VALUES ('5', '200', '1', '2');

INSERT INTO lessons (id, teacher_id, subject_id, class_id) VALUES ('6', '201', '2', '2');

INSERT INTO lesson_event_types (id, name) VALUES ('1', 'Common');

INSERT INTO lesson_event_types (id, name) VALUES ('2', 'Test');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('8', 'comment10', TRUE, '2017-06-28', '2017-06-28', '1');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('9', 'comment11', TRUE, '2017-06-29', '2017-06-29', '1');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('10', 'comment12', TRUE, '2017-06-30', '2017-06-30', '1');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('1', 'comment1', TRUE, '2017-07-03', '2017-07-03', '1');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('2', 'comment2', TRUE, '2017-07-04', '2017-07-04', '1');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('3', 'comment3', TRUE, '2017-07-05', '2017-07-05', '1');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('4', 'comment4', TRUE, '2017-07-06', '2017-07-06', '1');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('5', 'comment5', TRUE, '2017-07-07', '2017-07-07', '1');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('6', 'comment8', TRUE, '2017-07-10', '2017-07-10', '1');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('7', 'comment9', TRUE, '2017-07-11', '2017-07-11', '1');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('18', 'comment23', TRUE, '2017-06-28', '2017-06-28', '2');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('19', 'comment24', TRUE, '2017-06-29', '2017-06-29', '2');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('20', 'comment25', TRUE, '2017-06-30', '2017-06-30', '2');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('11', 'comment14', TRUE, '2017-07-03', '2017-07-03', '2');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('12', 'comment15', TRUE, '2017-07-04', '2017-07-04', '2');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('13', 'comment16', TRUE, '2017-07-05', '2017-07-05', '2');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('14', 'comment17', TRUE, '2017-07-06', '2017-07-06', '2');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('15', 'comment18', TRUE, '2017-07-07', '2017-07-07', '2');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('16', 'comment21', TRUE, '2017-07-10', '2017-07-10', '2');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('17', 'comment22', TRUE, '2017-07-11', '2017-07-11', '2');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('28', 'comment35', TRUE, '2017-06-28', '2017-06-28', '3');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('29', 'comment36', TRUE, '2017-06-29', '2017-06-29', '3');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('30', 'comment37', TRUE, '2017-06-30', '2017-06-30', '3');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('21', 'comment26', TRUE, '2017-07-03', '2017-07-03', '3');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('22', 'comment27', TRUE, '2017-07-04', '2017-07-04', '3');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('23', 'comment28', TRUE, '2017-07-05', '2017-07-05', '3');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('24', 'comment29', TRUE, '2017-07-06', '2017-07-06', '3');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('25', 'comment30', TRUE, '2017-07-07', '2017-07-07', '3');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('26', 'comment33', TRUE, '2017-07-10', '2017-07-10', '3');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('27', 'comment34', TRUE, '2017-07-11', '2017-07-11', '3');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('38', 'comment48', TRUE, '2017-06-28', '2017-06-28', '4');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('39', 'comment49', TRUE, '2017-06-29', '2017-06-29', '4');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('40', 'comment50', FALSE, '2017-06-30', '2017-06-30','4');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('31', 'comment39', TRUE, '2017-07-03', '2017-07-03', '4');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('32', 'comment40', TRUE, '2017-07-04', '2017-07-04', '4');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('33', 'comment41', FALSE , '2017-07-05', '2017-07-05','4');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('34', 'comment42', TRUE, '2017-07-06', '2017-07-06', '4');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('35', 'comment43', FALSE, '2017-07-07', '2017-07-07', '4');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('36', 'comment46', TRUE, '2017-07-10', '2017-07-10', '4');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('37', 'comment47', FALSE, '2017-07-11', '2017-07-11', '4');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('48', 'comment61', TRUE, '2017-06-28', '2017-06-28', '5');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('49', 'comment62', FALSE, '2017-06-29', '2017-06-29', '5');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('50', 'comment63', TRUE, '2017-06-30', '2017-06-30', '5');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('41', 'comment52', FALSE, '2017-07-03', '2017-07-03', '5');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('42', 'comment53', TRUE, '2017-07-04', '2017-07-04', '5');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('43', 'comment54', TRUE, '2017-07-05', '2017-07-05', '5');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('44', 'comment55', TRUE, '2017-07-06', '2017-07-06', '5');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('45', 'comment56', TRUE, '2017-07-07', '2017-07-07','5');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('46', 'comment59', TRUE, '2017-07-10', '2017-07-10', '5');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('47', 'comment60', TRUE, '2017-07-11', '2017-07-11', '5');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('58', 'comment74', TRUE, '2017-06-28', '2017-06-28', '6');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('59', 'comment75', TRUE, '2017-06-29', '2017-06-29', '6');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('60', 'comment76', TRUE, '2017-06-30', '2017-06-30', '6');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('51', 'comment65', TRUE, '2017-07-03', '2017-07-03', '6');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('52', 'comment66', TRUE, '2017-07-04', '2017-07-04', '6');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('53', 'comment67', TRUE, '2017-07-05', '2017-07-05', '6');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('54', 'comment68', TRUE, '2017-07-06', '2017-07-06', '6');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('55', 'comment69', TRUE, '2017-07-07', '2017-07-07', '6');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('56', 'comment72', TRUE, '2017-07-10', '2017-07-10', '6');

INSERT INTO lesson_events (id, comment, completed, event_date, completion_date, lesson_id) VALUES('57', 'comment73', TRUE, '2017-07-11', '2017-07-11', '6');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark) VALUES('1', TRUE,2, '207', '1',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark) ('2', FALSE,7, '204', '1',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('3', TRUE,8, '207', '2',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('4', FALSE,1, '204', '2',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('5', TRUE,4, '207', '3',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('6', TRUE,6, '204', '3',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('7', TRUE,6, '207', '4',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('8', FALSE,9, '204', '4',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('9', FALSE,8, '207', '5',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('10', FALSE,4, '204', '5',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('11', TRUE,9, '207', '6',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('12', TRUE,1, '204', '6',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('13', FALSE,8, '207', '7',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('14', FALSE,9, '204', '7',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('15', TRUE,0, '207', '8',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('16', TRUE,4, '204', '8',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('17', FALSE,10, '207', '9',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('18', FALSE,6, '204', '9',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('19', FALSE,7, '207', '10',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('20', FALSE,4, '204', '10',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('21', FALSE,5, '207', '11',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('22', FALSE,9, '204', '11',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('23', TRUE,3, '207', '12',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('24', FALSE,4, '204', '12',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('25', FALSE,11, '207', '13',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('26', FALSE,11, '204', '13',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('27', FALSE,1, '207', '14',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('28', FALSE,3, '204', '14',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('29', FALSE,8, '207', '15',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('30', TRUE,5, '204', '15',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('31', TRUE,10, '207', '16',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('32', FALSE,10, '204', '16',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('33', TRUE,0, '207', '17',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('34', FALSE,5, '204', '17',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('35', FALSE,3, '207', '18',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('36', TRUE,5, '204', '18',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('37', FALSE,8, '207', '19',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('38', FALSE,5, '204', '19',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('39', FALSE,2, '207', '20',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('40', FALSE,3, '204', '20',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('41', FALSE,8, '207', '21',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('42', FALSE,7, '204', '21',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('43', TRUE,3, '207', '22',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('44', TRUE,10, '204', '22',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('45', FALSE,11, '207', '23',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('46', FALSE,1, '204', '23',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('47', TRUE,10, '207', '24',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('48', FALSE,2, '204', '24',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('49', FALSE,9, '207', '25',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('50', FALSE,0, '204', '25',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('51', TRUE,0, '207', '26',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('52', FALSE,11, '204', '26',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('53', FALSE,5, '207', '27',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('54', TRUE,2, '204', '27',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('55', FALSE,3, '207', '28',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('56', FALSE,3, '204', '28',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('57', TRUE,6, '207', '29',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('58', FALSE,9, '204', '29',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('59', FALSE,6, '205', '30',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('60', FALSE,10, '206', '30',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('61', TRUE,4, '205', '31',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('62', TRUE,2, '206', '31',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('63', FALSE,11, '205', '32',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('64', FALSE,9, '206', '32',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('65', FALSE,10, '205', '33',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('66', TRUE,4, '206', '33',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('67', FALSE,6, '205', '34',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('68', TRUE,1, '206', '34',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('69', FALSE,1, '205', '35',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('70', FALSE,6, '206', '35',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('71', FALSE,8, '205', '36',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('72', TRUE,0, '206', '36',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('73', FALSE,5, '205', '37',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('74', FALSE,5, '206', '37',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('75', TRUE,3, '205', '38',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('76', FALSE,9, '206', '38',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('77', FALSE,8, '205', '39',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('78', TRUE,4, '206', '39',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('79', TRUE,5, '205', '40',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('80', FALSE,0, '206', '40',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('81', FALSE,6, '205', '41',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('82', FALSE,11, '206', '41',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('83', TRUE,12, '205', '42',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('84', FALSE,0, '206', '42',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('85', FALSE,9, '205', '43',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('86', FALSE,11, '206', '43',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('87', FALSE,12, '205', '44',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('88', FALSE,4, '206', '44',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('89', FALSE,1, '205', '45',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('90', TRUE,9, '206', '45',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('91', TRUE,3, '205', '46',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('92', TRUE,1, '206', '46',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('93', FALSE,8, '205', '47',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('94', TRUE,12, '206', '47',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('95', FALSE,5, '205', '48',1,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('96', TRUE,11, '206', '48',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('97', TRUE,2, '205', '49',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('98', FALSE,9, '206', '49',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('99', TRUE,7, '205', '50',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('100', FALSE,5, '206', '50',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('101', FALSE,2, '205', '51',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('102', FALSE,8, '206', '51',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('103', TRUE,1, '205', '52',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('104', FALSE,3, '206', '52',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('105', TRUE,0, '205', '53',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('106', TRUE,0, '206', '53',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('107', FALSE,9, '205', '54',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('108', FALSE,11, '206', '54',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('109', FALSE,6, '205', '55',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('110', FALSE,1, '206', '55',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('111', FALSE,12, '205', '56',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('112', FALSE,0, '206', '56',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('113', FALSE,0, '205', '57',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('114', TRUE,7, '206', '57',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('115', FALSE,10, '205', '58',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('116', TRUE,8, '206', '58',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('117', FALSE,11, '205', '59',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('118', TRUE,10, '206', '59',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('119', FALSE,9, '205', '60',2,'Done');

INSERT INTO child_marks (id, absent, mark, child_id, lesson_event_id,lessoneventtype_id,remark)('120', FALSE,3, '206', '60',1,'Done');

INSERT INTO file_resources VALUES('1', 'file1.txt', '/math/2017-06-20/alkghaigy1');

INSERT INTO file_resources VALUES('2', 'file2.ppt', '/math/2017-06-20/alkghaigy2');

INSERT INTO file_resources VALUES('3', 'file3.ppt', '/math/2017-06-20/alkghaigy3');

INSERT INTO file_resources VALUES('4', 'file4.ppt', '/math/2017-06-20/alkghaigy4');

INSERT INTO file_resources VALUES('5', 'file5.ppt', '/math/2017-06-20/alkghaigy5');

INSERT INTO file_event VALUES('1', '1');

INSERT INTO file_event VALUES('2', '2');

INSERT INTO file_event VALUES('3', '7');

INSERT INTO file_event VALUES('4', '7');

INSERT INTO file_event VALUES('5', '17');
